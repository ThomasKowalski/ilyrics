namespace iLyrics
{
    partial class iLyrics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components!= null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(iLyrics));
            this.btnGetInfo = new System.Windows.Forms.Button();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.cbArtworkService = new System.Windows.Forms.ComboBox();
            this.lblArtworkService = new System.Windows.Forms.Label();
            this.cbService = new System.Windows.Forms.ComboBox();
            this.lblLyricsService = new System.Windows.Forms.Label();
            this.chkLyrics = new System.Windows.Forms.CheckBox();
            this.chkAlbumArt = new System.Windows.Forms.CheckBox();
            this.chkAuto = new System.Windows.Forms.CheckBox();
            this.checkCheckSongs = new System.Windows.Forms.CheckBox();
            this.chkOverwrite = new System.Windows.Forms.CheckBox();
            this.pnlServiceOptions = new System.Windows.Forms.Panel();
            this.cbAdvanced = new System.Windows.Forms.CheckBox();
            this.pnlMore = new System.Windows.Forms.Panel();
            this.cbOverwriteArtwork = new System.Windows.Forms.CheckBox();
            this.bSettings = new System.Windows.Forms.Button();
            this.cbCreatePlaylistNotFound = new System.Windows.Forms.CheckBox();
            this.cbCreatePlaylistFound = new System.Windows.Forms.CheckBox();
            this.bGenerateSkipWhenShuffle = new System.Windows.Forms.Button();
            this.bResetArtwork = new System.Windows.Forms.Button();
            this.bOpenConsole = new System.Windows.Forms.Button();
            this.bCreateNotFoundOnDiskPlaylist = new System.Windows.Forms.Button();
            this.bApplyLyricsToSelected = new System.Windows.Forms.Button();
            this.bResetLyrics = new System.Windows.Forms.Button();
            this.pbProgress = new System.Windows.Forms.ProgressBar();
            this.bGenerateNoArtworkPlaylist = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.checkInternet = new System.ComponentModel.BackgroundWorker();
            this.workerUpdates = new System.ComponentModel.BackgroundWorker();
            this.pnlUpdates = new System.Windows.Forms.Panel();
            this.lblCheckUpdates = new System.Windows.Forms.Label();
            this.lblHelp = new System.Windows.Forms.Label();
            this.workerTasks = new System.ComponentModel.BackgroundWorker();
            this.workerUpdatesAuto = new System.ComponentModel.BackgroundWorker();
            this.pnlMain.SuspendLayout();
            this.pnlServiceOptions.SuspendLayout();
            this.pnlMore.SuspendLayout();
            this.pnlUpdates.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGetInfo
            // 
            this.btnGetInfo.Location = new System.Drawing.Point(559, 6);
            this.btnGetInfo.Name = "btnGetInfo";
            this.btnGetInfo.Size = new System.Drawing.Size(104, 40);
            this.btnGetInfo.TabIndex = 5;
            this.btnGetInfo.Text = "Get lyrics";
            this.btnGetInfo.UseVisualStyleBackColor = true;
            this.btnGetInfo.Click += new System.EventHandler(this.btnGetInfo_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.cbArtworkService);
            this.pnlMain.Controls.Add(this.lblArtworkService);
            this.pnlMain.Controls.Add(this.cbService);
            this.pnlMain.Controls.Add(this.lblLyricsService);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(675, 72);
            this.pnlMain.TabIndex = 0;
            // 
            // cbArtworkService
            // 
            this.cbArtworkService.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbArtworkService.Enabled = false;
            this.cbArtworkService.FormattingEnabled = true;
            this.cbArtworkService.Items.AddRange(new object[] {
            "Deezer"});
            this.cbArtworkService.Location = new System.Drawing.Point(100, 42);
            this.cbArtworkService.Name = "cbArtworkService";
            this.cbArtworkService.Size = new System.Drawing.Size(563, 21);
            this.cbArtworkService.TabIndex = 1;
            // 
            // lblArtworkService
            // 
            this.lblArtworkService.AutoSize = true;
            this.lblArtworkService.Location = new System.Drawing.Point(9, 45);
            this.lblArtworkService.Name = "lblArtworkService";
            this.lblArtworkService.Size = new System.Drawing.Size(85, 13);
            this.lblArtworkService.TabIndex = 2;
            this.lblArtworkService.Text = "Artwork Service:";
            // 
            // cbService
            // 
            this.cbService.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbService.FormattingEnabled = true;
            this.cbService.Items.AddRange(new object[] {
            "(Automatic)",
            "LyricWiki",
            "AZLyrics",
            "Genius"});
            this.cbService.Location = new System.Drawing.Point(100, 12);
            this.cbService.Name = "cbService";
            this.cbService.Size = new System.Drawing.Size(563, 21);
            this.cbService.TabIndex = 0;
            // 
            // lblLyricsService
            // 
            this.lblLyricsService.AutoSize = true;
            this.lblLyricsService.Location = new System.Drawing.Point(9, 15);
            this.lblLyricsService.Name = "lblLyricsService";
            this.lblLyricsService.Size = new System.Drawing.Size(71, 13);
            this.lblLyricsService.TabIndex = 0;
            this.lblLyricsService.Text = "Lyric Service:";
            // 
            // chkLyrics
            // 
            this.chkLyrics.AutoSize = true;
            this.chkLyrics.Checked = true;
            this.chkLyrics.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLyrics.Location = new System.Drawing.Point(12, 17);
            this.chkLyrics.Name = "chkLyrics";
            this.chkLyrics.Size = new System.Drawing.Size(69, 17);
            this.chkLyrics.TabIndex = 0;
            this.chkLyrics.Text = "Get lyrics";
            this.chkLyrics.UseVisualStyleBackColor = true;
            this.chkLyrics.CheckedChanged += new System.EventHandler(this.chkLyrics_CheckedChanged);
            // 
            // chkAlbumArt
            // 
            this.chkAlbumArt.AutoSize = true;
            this.chkAlbumArt.Location = new System.Drawing.Point(100, 17);
            this.chkAlbumArt.Name = "chkAlbumArt";
            this.chkAlbumArt.Size = new System.Drawing.Size(89, 17);
            this.chkAlbumArt.TabIndex = 1;
            this.chkAlbumArt.Text = "Get album art";
            this.chkAlbumArt.UseVisualStyleBackColor = true;
            this.chkAlbumArt.CheckedChanged += new System.EventHandler(this.chkAlbumArt_CheckedChanged);
            // 
            // chkAuto
            // 
            this.chkAuto.AutoSize = true;
            this.chkAuto.Location = new System.Drawing.Point(196, 17);
            this.chkAuto.Name = "chkAuto";
            this.chkAuto.Size = new System.Drawing.Size(126, 17);
            this.chkAuto.TabIndex = 2;
            this.chkAuto.Text = "Update Automatically";
            this.chkAuto.UseVisualStyleBackColor = true;
            // 
            // checkCheckSongs
            // 
            this.checkCheckSongs.AutoSize = true;
            this.checkCheckSongs.Location = new System.Drawing.Point(12, 20);
            this.checkCheckSongs.Name = "checkCheckSongs";
            this.checkCheckSongs.Size = new System.Drawing.Size(96, 17);
            this.checkCheckSongs.TabIndex = 0;
            this.checkCheckSongs.Text = "Reverify songs";
            this.checkCheckSongs.UseVisualStyleBackColor = true;
            // 
            // chkOverwrite
            // 
            this.chkOverwrite.AutoSize = true;
            this.chkOverwrite.Location = new System.Drawing.Point(12, 100);
            this.chkOverwrite.Name = "chkOverwrite";
            this.chkOverwrite.Size = new System.Drawing.Size(97, 17);
            this.chkOverwrite.TabIndex = 3;
            this.chkOverwrite.Text = "Overwrite lyrics";
            this.chkOverwrite.UseVisualStyleBackColor = true;
            // 
            // pnlServiceOptions
            // 
            this.pnlServiceOptions.Controls.Add(this.cbAdvanced);
            this.pnlServiceOptions.Controls.Add(this.chkAlbumArt);
            this.pnlServiceOptions.Controls.Add(this.chkLyrics);
            this.pnlServiceOptions.Controls.Add(this.chkAuto);
            this.pnlServiceOptions.Controls.Add(this.btnGetInfo);
            this.pnlServiceOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlServiceOptions.Location = new System.Drawing.Point(0, 72);
            this.pnlServiceOptions.Name = "pnlServiceOptions";
            this.pnlServiceOptions.Size = new System.Drawing.Size(675, 50);
            this.pnlServiceOptions.TabIndex = 1;
            // 
            // cbAdvanced
            // 
            this.cbAdvanced.AutoSize = true;
            this.cbAdvanced.Location = new System.Drawing.Point(430, 17);
            this.cbAdvanced.Name = "cbAdvanced";
            this.cbAdvanced.Size = new System.Drawing.Size(104, 17);
            this.cbAdvanced.TabIndex = 4;
            this.cbAdvanced.Text = "Show advanced";
            this.cbAdvanced.UseVisualStyleBackColor = true;
            this.cbAdvanced.CheckedChanged += new System.EventHandler(this.cbAdvanced_CheckedChanged);
            // 
            // pnlMore
            // 
            this.pnlMore.Controls.Add(this.cbOverwriteArtwork);
            this.pnlMore.Controls.Add(this.bSettings);
            this.pnlMore.Controls.Add(this.cbCreatePlaylistNotFound);
            this.pnlMore.Controls.Add(this.cbCreatePlaylistFound);
            this.pnlMore.Controls.Add(this.chkOverwrite);
            this.pnlMore.Controls.Add(this.bGenerateSkipWhenShuffle);
            this.pnlMore.Controls.Add(this.bResetArtwork);
            this.pnlMore.Controls.Add(this.bOpenConsole);
            this.pnlMore.Controls.Add(this.checkCheckSongs);
            this.pnlMore.Controls.Add(this.bCreateNotFoundOnDiskPlaylist);
            this.pnlMore.Controls.Add(this.bApplyLyricsToSelected);
            this.pnlMore.Controls.Add(this.bResetLyrics);
            this.pnlMore.Controls.Add(this.pbProgress);
            this.pnlMore.Controls.Add(this.bGenerateNoArtworkPlaylist);
            this.pnlMore.Controls.Add(this.button1);
            this.pnlMore.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMore.Location = new System.Drawing.Point(0, 122);
            this.pnlMore.Name = "pnlMore";
            this.pnlMore.Size = new System.Drawing.Size(675, 180);
            this.pnlMore.TabIndex = 2;
            // 
            // cbOverwriteArtwork
            // 
            this.cbOverwriteArtwork.AutoSize = true;
            this.cbOverwriteArtwork.Location = new System.Drawing.Point(12, 123);
            this.cbOverwriteArtwork.Name = "cbOverwriteArtwork";
            this.cbOverwriteArtwork.Size = new System.Drawing.Size(109, 17);
            this.cbOverwriteArtwork.TabIndex = 12;
            this.cbOverwriteArtwork.Text = "Overwrite artwork";
            this.cbOverwriteArtwork.UseVisualStyleBackColor = true;
            // 
            // bSettings
            // 
            this.bSettings.Location = new System.Drawing.Point(335, 100);
            this.bSettings.Name = "bSettings";
            this.bSettings.Size = new System.Drawing.Size(158, 41);
            this.bSettings.TabIndex = 8;
            this.bSettings.Text = "Settings";
            this.bSettings.UseVisualStyleBackColor = true;
            this.bSettings.Click += new System.EventHandler(this.bSettings_Click);
            // 
            // cbCreatePlaylistNotFound
            // 
            this.cbCreatePlaylistNotFound.AutoSize = true;
            this.cbCreatePlaylistNotFound.Location = new System.Drawing.Point(12, 66);
            this.cbCreatePlaylistNotFound.Name = "cbCreatePlaylistNotFound";
            this.cbCreatePlaylistNotFound.Size = new System.Drawing.Size(151, 17);
            this.cbCreatePlaylistNotFound.TabIndex = 2;
            this.cbCreatePlaylistNotFound.Text = "Create \"Not found\" playlist";
            this.cbCreatePlaylistNotFound.UseVisualStyleBackColor = true;
            // 
            // cbCreatePlaylistFound
            // 
            this.cbCreatePlaylistFound.AutoSize = true;
            this.cbCreatePlaylistFound.Location = new System.Drawing.Point(12, 43);
            this.cbCreatePlaylistFound.Name = "cbCreatePlaylistFound";
            this.cbCreatePlaylistFound.Size = new System.Drawing.Size(134, 17);
            this.cbCreatePlaylistFound.TabIndex = 1;
            this.cbCreatePlaylistFound.Text = "Create \"Found\" playlist";
            this.cbCreatePlaylistFound.UseVisualStyleBackColor = true;
            // 
            // bGenerateSkipWhenShuffle
            // 
            this.bGenerateSkipWhenShuffle.Location = new System.Drawing.Point(335, 53);
            this.bGenerateSkipWhenShuffle.Name = "bGenerateSkipWhenShuffle";
            this.bGenerateSkipWhenShuffle.Size = new System.Drawing.Size(158, 41);
            this.bGenerateSkipWhenShuffle.TabIndex = 7;
            this.bGenerateSkipWhenShuffle.Text = "Generate \"Don\'t Skip when shuffle\" playlist";
            this.bGenerateSkipWhenShuffle.UseVisualStyleBackColor = true;
            this.bGenerateSkipWhenShuffle.Click += new System.EventHandler(this.bGenerateSkipWhenShuffle_Click);
            // 
            // bResetArtwork
            // 
            this.bResetArtwork.Location = new System.Drawing.Point(505, 53);
            this.bResetArtwork.Name = "bResetArtwork";
            this.bResetArtwork.Size = new System.Drawing.Size(158, 41);
            this.bResetArtwork.TabIndex = 10;
            this.bResetArtwork.Text = "Reset artwork";
            this.bResetArtwork.UseVisualStyleBackColor = true;
            this.bResetArtwork.Click += new System.EventHandler(this.bResetArtwork_Click);
            // 
            // bOpenConsole
            // 
            this.bOpenConsole.Location = new System.Drawing.Point(505, 100);
            this.bOpenConsole.Name = "bOpenConsole";
            this.bOpenConsole.Size = new System.Drawing.Size(158, 41);
            this.bOpenConsole.TabIndex = 11;
            this.bOpenConsole.Text = "Open console";
            this.bOpenConsole.UseVisualStyleBackColor = true;
            this.bOpenConsole.Click += new System.EventHandler(this.bOpenConsole_Click);
            // 
            // bCreateNotFoundOnDiskPlaylist
            // 
            this.bCreateNotFoundOnDiskPlaylist.Location = new System.Drawing.Point(164, 100);
            this.bCreateNotFoundOnDiskPlaylist.Name = "bCreateNotFoundOnDiskPlaylist";
            this.bCreateNotFoundOnDiskPlaylist.Size = new System.Drawing.Size(158, 41);
            this.bCreateNotFoundOnDiskPlaylist.TabIndex = 5;
            this.bCreateNotFoundOnDiskPlaylist.Text = "Create \"Not found on disk\" playlist";
            this.bCreateNotFoundOnDiskPlaylist.UseVisualStyleBackColor = true;
            this.bCreateNotFoundOnDiskPlaylist.Click += new System.EventHandler(this.bCreateNotFoundOnDiskPlaylist_Click);
            // 
            // bApplyLyricsToSelected
            // 
            this.bApplyLyricsToSelected.Location = new System.Drawing.Point(335, 6);
            this.bApplyLyricsToSelected.Name = "bApplyLyricsToSelected";
            this.bApplyLyricsToSelected.Size = new System.Drawing.Size(158, 41);
            this.bApplyLyricsToSelected.TabIndex = 6;
            this.bApplyLyricsToSelected.Text = "Set lyrics to selected";
            this.bApplyLyricsToSelected.UseVisualStyleBackColor = true;
            this.bApplyLyricsToSelected.Click += new System.EventHandler(this.bApplyLyricsToSelected_Click);
            // 
            // bResetLyrics
            // 
            this.bResetLyrics.Location = new System.Drawing.Point(505, 6);
            this.bResetLyrics.Name = "bResetLyrics";
            this.bResetLyrics.Size = new System.Drawing.Size(158, 41);
            this.bResetLyrics.TabIndex = 9;
            this.bResetLyrics.Text = "Reset lyrics";
            this.bResetLyrics.UseVisualStyleBackColor = true;
            this.bResetLyrics.Click += new System.EventHandler(this.bResetLyrics_Click);
            // 
            // pbProgress
            // 
            this.pbProgress.Location = new System.Drawing.Point(12, 150);
            this.pbProgress.Name = "pbProgress";
            this.pbProgress.Size = new System.Drawing.Size(651, 23);
            this.pbProgress.TabIndex = 6;
            // 
            // bGenerateNoArtworkPlaylist
            // 
            this.bGenerateNoArtworkPlaylist.Location = new System.Drawing.Point(164, 53);
            this.bGenerateNoArtworkPlaylist.Name = "bGenerateNoArtworkPlaylist";
            this.bGenerateNoArtworkPlaylist.Size = new System.Drawing.Size(158, 41);
            this.bGenerateNoArtworkPlaylist.TabIndex = 4;
            this.bGenerateNoArtworkPlaylist.Text = "Generate \"No Artwork\" Playlist";
            this.bGenerateNoArtworkPlaylist.UseVisualStyleBackColor = true;
            this.bGenerateNoArtworkPlaylist.Click += new System.EventHandler(this.bGenerateNoArtworkPlaylist_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(164, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(158, 41);
            this.button1.TabIndex = 3;
            this.button1.Text = "Generate \"No lyrics\" playlist";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.bGenerateNoLyrics_Click);
            // 
            // checkInternet
            // 
            this.checkInternet.DoWork += new System.ComponentModel.DoWorkEventHandler(this.checkInternet_DoWork);
            // 
            // workerUpdates
            // 
            this.workerUpdates.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerUpdates_DoWork);
            // 
            // pnlUpdates
            // 
            this.pnlUpdates.AutoSize = true;
            this.pnlUpdates.Controls.Add(this.lblCheckUpdates);
            this.pnlUpdates.Controls.Add(this.lblHelp);
            this.pnlUpdates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUpdates.Location = new System.Drawing.Point(0, 302);
            this.pnlUpdates.Name = "pnlUpdates";
            this.pnlUpdates.Size = new System.Drawing.Size(675, 50);
            this.pnlUpdates.TabIndex = 3;
            // 
            // lblCheckUpdates
            // 
            this.lblCheckUpdates.AutoSize = true;
            this.lblCheckUpdates.Location = new System.Drawing.Point(12, 26);
            this.lblCheckUpdates.Name = "lblCheckUpdates";
            this.lblCheckUpdates.Size = new System.Drawing.Size(271, 13);
            this.lblCheckUpdates.TabIndex = 1;
            this.lblCheckUpdates.Text = "Want new, cool features ? Click here to check updates!";
            this.lblCheckUpdates.Click += new System.EventHandler(this.lblCheckUpdates_Click);
            // 
            // lblHelp
            // 
            this.lblHelp.AutoSize = true;
            this.lblHelp.Location = new System.Drawing.Point(12, 10);
            this.lblHelp.Name = "lblHelp";
            this.lblHelp.Size = new System.Drawing.Size(366, 13);
            this.lblHelp.TabIndex = 0;
            this.lblHelp.Text = "For help, please head to bitbucket.org/thomaskowalski/ilyrics (or click here!)";
            this.lblHelp.Click += new System.EventHandler(this.lblHelp_Click);
            // 
            // workerTasks
            // 
            this.workerTasks.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerTasks_DoWork);
            // 
            // workerUpdatesAuto
            // 
            this.workerUpdatesAuto.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerUpdatesAuto_DoWork);
            // 
            // iLyrics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(675, 352);
            this.Controls.Add(this.pnlUpdates);
            this.Controls.Add(this.pnlMore);
            this.Controls.Add(this.pnlServiceOptions);
            this.Controls.Add(this.pnlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "iLyrics";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "-";
            this.Load += new System.EventHandler(this.iLyrics_Load);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.pnlServiceOptions.ResumeLayout(false);
            this.pnlServiceOptions.PerformLayout();
            this.pnlMore.ResumeLayout(false);
            this.pnlMore.PerformLayout();
            this.pnlUpdates.ResumeLayout(false);
            this.pnlUpdates.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetInfo;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.CheckBox chkAuto;
        private System.Windows.Forms.CheckBox chkOverwrite;
        private System.Windows.Forms.Panel pnlServiceOptions;
        private System.Windows.Forms.Label lblLyricsService;
        private System.Windows.Forms.ComboBox cbService;
        private System.Windows.Forms.Panel pnlMore;
        private System.Windows.Forms.CheckBox checkCheckSongs;
        private System.Windows.Forms.CheckBox chkAlbumArt;
        private System.ComponentModel.BackgroundWorker checkInternet;
        private System.Windows.Forms.CheckBox chkLyrics;
        private System.ComponentModel.BackgroundWorker workerUpdates;
        private System.Windows.Forms.Panel pnlUpdates;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblCheckUpdates;
        private System.Windows.Forms.Label lblHelp;
        private System.Windows.Forms.Button bGenerateNoArtworkPlaylist;
        private System.Windows.Forms.ProgressBar pbProgress;
        private System.ComponentModel.BackgroundWorker workerTasks;
        private System.Windows.Forms.Button bResetLyrics;
        private System.Windows.Forms.Button bApplyLyricsToSelected;
        private System.Windows.Forms.Button bCreateNotFoundOnDiskPlaylist;
        private System.Windows.Forms.CheckBox cbAdvanced;
        private System.Windows.Forms.Button bOpenConsole;
        private System.Windows.Forms.Button bResetArtwork;
        private System.Windows.Forms.Button bGenerateSkipWhenShuffle;
        private System.Windows.Forms.CheckBox cbCreatePlaylistNotFound;
        private System.Windows.Forms.CheckBox cbCreatePlaylistFound;
        private System.Windows.Forms.ComboBox cbArtworkService;
        private System.Windows.Forms.Label lblArtworkService;
        private System.Windows.Forms.Button bSettings;
        private System.Windows.Forms.CheckBox cbOverwriteArtwork;
        private System.ComponentModel.BackgroundWorker workerUpdatesAuto;

    }
}

