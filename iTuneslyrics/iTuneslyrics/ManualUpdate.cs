using System;
using System.Windows.Forms;
using System.Reflection;
using iTunesLib;
using iLyrics.Interface;
using System.Collections.Generic;

namespace iLyrics
{
    partial class ManualUpdate : Form
    {
        public ILyricService LyricService;
        public IArtworkService ArtFinder;

        public Boolean getArtwork;
        public Boolean overwriteArtwork = false; //We can't do this, unfortunately. 

        public Boolean getLyrics;

        public IITPlaylist NotFoundPlaylist;

        public IITFileOrCDTrack CurrentTrack;
        public ManualUpdate()
        {
            InitializeComponent();
        }

        private void ManualUpdate_Load(object sender, EventArgs e)
        {
            try
            {
                string tempPath = String.Empty;
                string artist = CurrentTrack.Artist;
                string song = CurrentTrack.Name;
                string album = CurrentTrack.Album;

                if (artist == null)
                    artist = "";
                if (album == null)
                    album = "";

                lblArtist.Text = artist;
                lblSong.Text = song;
                lblAlbum.Text = album;

                if (CurrentTrack.Artwork[1] != null && !overwriteArtwork)
                {
                    tempPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\itunesart";
                    CurrentTrack.Artwork[1].SaveArtworkToFile(tempPath);
                    artPictureBox.ImageLocation = tempPath;
                }
                else if (getArtwork) // If the user stated he wanted to get the artwork
                {
                    if (ArtFinder.fetchArtwork(CurrentTrack.Artist, CurrentTrack.Album)) //We ask the artFinder to get it and if he succeeds
                        //Update the PictureBox
                        artPictureBox.ImageLocation = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\" + artist + " " + CurrentTrack.Album + ".jpg";
                }

                if (LyricService != null)
                {
                    string lyrics;

                    var result = FetchLyrics(artist, song, out lyrics);

                    if (result == SearchResult.NotFound)
                    {
                        //remove parenthesis (and any thing inside) and retry;
                        List<String> symbols = new List<String>();
                        symbols.Add("(,)");
                        symbols.Add("[,]");
                        symbols.Add("{,}");
                        foreach (string symbolCouple in symbols)
                        {
                            string symbol1 = symbolCouple.Split(',')[0];
                            string symbol2 = symbolCouple.Split(',')[1];
                            var parenStartIndex = song.IndexOf(symbol1);
                            var parenEndIndex = song.IndexOf(symbol2);

                            if (parenStartIndex > -1 && parenEndIndex > -1)
                            {
                                song = (song.Remove(parenStartIndex, parenEndIndex - parenStartIndex+1)).Trim();
                            }
                        }
                        result = FetchLyrics(artist, song, out lyrics);
                    }


                    if (result == SearchResult.InvalidKey)
                    {
                        //The API Token is invalid
                        MessageBox.Show("The Genius API Key you're using is invalid! Please update it in the Settings window (you can learn how to get a new one there) and start over.", "Invalid token", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Close();
                        Dispose();
                    }
                    if (result == SearchResult.Found)
                    {
                        lyricsBox.Text = lyrics;
                    }
                    else
                    {
                        lyricsBox.Text = "No results found for this song for service <" + LyricService.GetName() + ">";
                    }

                    btnUpdate.Enabled = result == SearchResult.Found;
                }
            }
            catch (Exception ex)
            {
                if (LyricService != null)
                    lyricsBox.Text = "Error in service " + LyricService.GetName() + ":\r\n" + ex.Message;
                else
                    lyricsBox.Text = "Error:\r\n" + ex.Message;

                btnUpdate.Enabled = false;
            }

        }

        private SearchResult FetchLyrics(string artist, string song, out string lyrics)
        {
            string url;
            var result = LyricService.Exists(artist, song, out url);
            if (result == SearchResult.NotFound || result == SearchResult.InvalidKey || result == SearchResult.OtherError)
            {
                lyrics = string.Empty;
                return result;
            }
            return LyricService.GetLyrics(artist, song, out lyrics, url);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (getArtwork)
            {
                if (System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\" + CurrentTrack.Artist + " " + CurrentTrack.Album + ".jpg"))
                    CurrentTrack.AddArtworkFromFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\" + CurrentTrack.Artist + " " + CurrentTrack.Album + ".jpg"); //We apply it
            }
            if (getLyrics)
                CurrentTrack.Lyrics = lyricsBox.Text;
            Close();
            Dispose();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
            Close();
            Dispose();
        }

        public bool overWriteLyrics { get; set; }
    }
}