﻿/*
 * AZLyrics class by Thomas Kowalski
 * Using azlyrics.com resources
 * Please include a link to https://bitbucket.org/ThomasKowalski/ilyrics and to http://thomaskowalski.net if you use it
 * For any help, send me an e-mail to thomaskowalski [at] outlook [com]
 * And it's not authorized for commercial use. Sorry.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using iLyrics.Interface;
using HtmlAgilityPack;

namespace iLyrics
{
    class AZLyrics : ILyricService
    {
        private List<int> _listIgnore;
        private bool ignore = false;

        WebClient wc;
        public AZLyrics(List<int> listIgnore)
        {
            if (listIgnore == null)
            {
                ignore = true;
                _listIgnore = null;
            }
            else
                _listIgnore = listIgnore;

            wc = new WebClient();
        }

        public string GetName()
        {
            return "AZLyrics";
        }

        public SearchResult Exists(string artist, string song, out string url)
        {
            if (!ignore && _listIgnore.IndexOf(artist.GetHashCode() * song.GetHashCode()) != -1)
            {
                url = null;
                return SearchResult.NotFound;
            }

            wc.Encoding = System.Text.Encoding.UTF8;
            string webpageContent;
            string cx = "016839032996308921873:mcvdflsaxwq";
            string searchURL = "https://www.googleapis.com/customsearch/v1?searchtype=image&key=" + (new SettingsManager()).GoogleAPIKey + "&cx=" + cx + "&q=";
            try
            {
                webpageContent = wc.DownloadString(searchURL + artist + " " + song);
            }
            catch (Exception)
            {
                url = null;
                return SearchResult.NotFound;
            }

            Newtonsoft.Json.Linq.JObject response = Newtonsoft.Json.Linq.JObject.Parse(webpageContent);
            url = response["items"][0]["link"].ToString();
            return SearchResult.Found;
        }

        public SearchResult GetLyrics(string artist, string song, out string lyrics, string url = null)
        {
            WebClient wc = new WebClient();
            wc.Encoding = System.Text.Encoding.UTF8;

            lyrics = "";
            string lyricsUrl = "";
            string webpageContent = "";

            HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();

            // Search for the lyrics using Exists.
            if (!(Exists(artist, song, out lyricsUrl) == SearchResult.Found))
                return SearchResult.NotFound;

            try
            {
                webpageContent = wc.DownloadString(System.Uri.EscapeUriString(lyricsUrl));
            }
            catch (Exception)
            {
                lyrics = "";
                return SearchResult.NotFound;
            }

            htmlDoc.LoadHtml(webpageContent);

            foreach (var div in htmlDoc.DocumentNode.Descendants("div"))
            {
                if (div.InnerHtml.Contains("<!-- Usage of azlyrics.com content by any third-party lyrics provider is prohibited by our licensing agreement. Sorry about that. -->") && !div.InnerHtml.Contains("<!-- AddThis Button END -->"))
                {
                    System.Diagnostics.Debug.Print(div.InnerHtml);
                    lyrics = div.InnerText.Replace("<!-- Usage of azlyrics.com content by any third-party lyrics provider is prohibited by our licensing agreement. Sorry about that. -->", "");
                    break;
                }
            }

            if (lyrics == "")
                return SearchResult.NotFound;

            lyrics = removeHtmlTagging(lyrics);
            lyrics = lyrics.Trim('\r', '\n');
            lyrics = applyHtmlElements(lyrics);
            return SearchResult.Found;
        }

        private string applyHtmlElements(string text)
        {
            text = text.Replace("&quot;", "\"");
            text = text.Replace("&amt;", "&");
            text = text.Replace("&lt;", "<");
            text = text.Replace("&gt;", ">");
            return text;
        }

        private bool containsHtmlTagging(string text)
        {
            text = text.ToLower();
            List<string> tags = new List<string>(new string[] { "<i>", "</i>", "<br>", "<div>", "</div>", "<br />", "<br/>", "<b>", "</b>" });
            foreach (string tag in tags)
            {
                if (text.Contains(tag))
                    return true;
            }
            return false;
        }

        private string removeHtmlTagging(string text)
        {
            List<string> tags = new List<string>(new string[] { "<i>", "</i>", "<br>", "<div>", "</div>", "<br />", "<br/>", "<b>", "</b>" });
            foreach (string tag in tags)
            {
                text = text.Replace(tag, "");
            }
            return text;
        }

        private string normalize(string toNormalize)
        {
            if (toNormalize.Contains(" ft."))
                toNormalize = getToEnd(" ft.", toNormalize);
            if (toNormalize.Contains(" feat."))
                toNormalize = getToEnd(" feat.", toNormalize);
            if (toNormalize.Contains(" feat"))
                toNormalize = getToEnd(" feat", toNormalize);
            if (toNormalize.Contains(" ft"))
                toNormalize = getToEnd(" ft", toNormalize);
            if (toNormalize.Contains(" featuring"))
                toNormalize = getToEnd(" featuring", toNormalize);

            toNormalize = toNormalize.Replace(" ", "");
            toNormalize = toNormalize.Replace("$", "");

            return toNormalize.ToLower();
        }

        private string getBetweenTags(string tag1, string tag2, string text)
        {
            //Second delimiting word
            int nIndexStart = text.IndexOf(tag1);
            //Find the first occurrence of f1
            int nIndexEnd = text.IndexOf(tag2);
            //Find the first occurrence of f2

            //-1 means the word was not found.
            if (nIndexStart > -1 && nIndexEnd > -1)
            {
                string res = text.Substring(nIndexStart + tag1.Length, nIndexEnd - nIndexStart - tag1.Length);
                //Crop the text between
                return (res);
                //Display
            }
            else
            {
                return ("One or both of the delimiting words were not found!");
            }
        }

        private string getToEnd(string tag1, string text)
        {
            //Second delimiting word
            int nIndexStart = text.IndexOf(tag1);
            //Find the first occurrence of f1

            //-1 means the word was not found.
            if (nIndexStart > -1)
            {
                string res = text.Substring(0, nIndexStart);
                //Crop the text between
                return (res);
                //Display
            }
            else
            {
                return ("One or both of the delimiting words were not found!");
            }
        }
    }
}
