using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using iLyrics.com.wikia.lyrics;
using iLyrics.Interface;
using System.Collections.Generic;


namespace iLyrics.LyricService
{
    public class LyricWiki : ILyricService
    {

        private const string ISO_8859 = "ISO-8859-1";
        private static readonly Regex NewLineEx = new Regex("<br */?>", RegexOptions.Compiled);
        private const string NEW_LINE = "\r\n";
        private const string HTML_TAG_PATTERN = "<.*?>";
        private static readonly Regex LyricBoxEx = new Regex(@"<div class='lyricbox'>.*?</div>(.*?)<!--", RegexOptions.Compiled);

        private readonly com.wikia.lyrics.LyricWiki _lyricsWiki = new com.wikia.lyrics.LyricWiki();

        private List<int> _listIgnore;
        private bool ignore = false;

        public string GetName()
        {
            return "LyricWiki";
        }

        public LyricWiki(List<int> listIgnore)
        {
            if (listIgnore == null)
            {
                ignore = true;
                _listIgnore = null;
            }
            else
                _listIgnore = listIgnore;
        }
        public SearchResult Exists(string artist, string song, out string url)
        {
            url = null;
            if (_lyricsWiki.checkSongExists(artist, song))
                return SearchResult.Found;

            return SearchResult.NotFound;
        }

        public SearchResult GetLyrics(string artist, string song, out string lyrics, string url = null)
        {
            throw new Exception();
            lyrics = string.Empty;

            if (!ignore && _listIgnore.IndexOf(artist.GetHashCode() * song.GetHashCode()) != -1)
                return SearchResult.NotFound;

            LyricsResult result = _lyricsWiki.getSong(artist, song);
            Encoding iso8859 = Encoding.GetEncoding(ISO_8859); // thanks to davidreis

            string _url = Encoding.UTF8.GetString(iso8859.GetBytes(result.url));

            if (String.IsNullOrEmpty(result.lyrics))
            {
                string lyricsAttempt2;
                if (GetLyrics(artist, song.ToLower(), out lyricsAttempt2) == SearchResult.NotFound)
                {
                    _listIgnore.Add(song.GetHashCode() * artist.GetHashCode());
                    Others.writeIgnoreList("lyricwiki", _listIgnore);
                    return SearchResult.NotFound;
                }
                else
                {
                    lyrics = lyricsAttempt2;
                }
            }

            if (!String.IsNullOrEmpty(url))
            {
                lyrics = RetrieveSong(url);
                if (String.IsNullOrEmpty(lyrics)) return SearchResult.NotFound;
            }
            else
            {
                lyrics = result.lyrics;
                lyrics = HttpUtility.HtmlDecode(lyrics);
            }

            return SearchResult.Found;

        }

        private static string RetrieveSong(string url)
        {
            string lyrics = string.Empty;

            var webRequest = WebRequest.Create(url) as HttpWebRequest;
            string html;

            var response = webRequest.GetResponse();
            Encoding iso8859 = Encoding.GetEncoding(ISO_8859);

            using (var reader = new StreamReader(response.GetResponseStream(), iso8859))
            {
                html = reader.ReadToEnd();
            }

            Match m = LyricBoxEx.Match(html);

            if (!m.Success) return string.Empty;

            lyrics = m.Groups[1].Value.Trim();

            lyrics = lyrics.Replace("');var c=function(){cf.showAsyncAd(opts)};if(window.cf)c();else{cf_async=!0;var r=document.createElement(\"script\"),s=document.getElementsByTagName(\"script\")[0];r.async=!0;r.src=\"//srv.tonefuse.com/showads/showad.js\";r.readyState?r.onreadystatechange=function(){if(\"loaded\"==r.readyState||\"complete\"==r.readyState)r.onreadystatechange=null,c()}:r.onload=c;s.parentNode.insertBefore(r,s)};}})();", "");
            lyrics = NewLineEx.Replace(lyrics, NEW_LINE);
            lyrics = Regex.Replace(lyrics, HTML_TAG_PATTERN, string.Empty);
            lyrics = HttpUtility.HtmlDecode(lyrics);

            //hard-coded sleep of 2 seconds to not spam the lyrics wiki :(
            System.Threading.Thread.Sleep(2000);

            return lyrics;
        }

    }
}