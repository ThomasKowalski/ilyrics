﻿/*
 * Genius class by Thomas Kowalski
 * Using Genius.com resources
 * Please include a link to https://bitbucket.org/ThomasKowalski/ilyrics and to https://thomaskowalski.net if you use it
 * For any help, send me an e-mail to thomaskowalski [at] outlook [com]
 * And it's not authorized for commercial use. Sorry.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using iLyrics.Interface;
using Newtonsoft.Json.Linq;

namespace iLyrics
{
    class Genius : ILyricService
    {
        private List<int> _listIgnore;
        private bool ignore = false;

        WebClient wc;
        public Genius(List<int> listIgnore)
        {
            if (listIgnore == null)
            {
                ignore = true;
                _listIgnore = null;
            }
            else
                _listIgnore = listIgnore;

            wc = new WebClient();
        }

        public string GetName()
        {
            return "Genius";
        }

        public SearchResult Exists(string artist, string song, out string url)
        {
            if (!ignore && _listIgnore != null && _listIgnore.IndexOf(artist.GetHashCode() * song.GetHashCode()) != -1)
            {
                System.Diagnostics.Debug.Print("Ignored '" + song + "' by " + artist);
                url = null;
                return SearchResult.NotFound;
            }
            
            string webpageContent;
            try
            { 
                webpageContent = wc.DownloadString("http://api.genius.com/search?access_token=" + System.Uri.EscapeUriString((new SettingsManager()).GeniusAPIKey + "&q=" + song + " " + artist));
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.Print("Invalid Genius key!");
                url = null;
                return SearchResult.InvalidKey;
            }
            JObject response = JObject.Parse(webpageContent);
            try
            {
                string _url = response["response"]["hits"][0]["result"]["url"].ToString();

                // Spotify new songs filter
                if (!_url.ToLower().Contains("spotify"))
                {
                    url = _url;
                    System.Diagnostics.Debug.Print(url);
                    return SearchResult.Found;
                }
                else
                {
                    _listIgnore.Add(song.GetHashCode() * artist.GetHashCode());
                    Others.writeIgnoreList("genius", _listIgnore);
                    url = null;
                    System.Diagnostics.Debug.Print("Pas trouvé !");
                    return SearchResult.NotFound;
                }

            }
            catch (Exception)
            {
                System.Diagnostics.Debug.Print("Exception in Genius!");
                url = null;
                return SearchResult.InvalidKey;
            }
        }

        public SearchResult GetLyrics(string artist, string song, out string lyrics, string url = null)
        {
            try
            {
                wc.Encoding = Encoding.UTF8;
                string webpageContent = wc.DownloadString(url);

                lyrics = webpageContent;
                HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.LoadHtml(lyrics);
                foreach (var div in htmlDoc.DocumentNode.Descendants("div"))
                {
                    try
                    {
                        if (!div.Attributes.Contains("class"))
                            continue;
                        if (div.Attributes["class"].Value.ToString() == "lyrics")
                        {
                            if (div.InnerText.Length < 10000)
                            {
                                lyrics = div.InnerText.Replace("<!--/sse-->", "").Replace("<!--sse-->", "").Trim();
                                return SearchResult.Found;
                            }
                            else
                            {
                                lyrics = "";
                                return SearchResult.NotFound;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            System.Diagnostics.Debug.Print(div.Attributes["class"].Value.ToString());
                        }
                        catch (Exception e2)
                        {
                            System.Diagnostics.Debug.Print("In Genius service:");
                            System.Diagnostics.Debug.Print(e.Message);
                            System.Diagnostics.Debug.Print(e2.Message);
                        }
                    }
                }
                return SearchResult.NotFound;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Print(e.Message);
                lyrics = "";
                return SearchResult.OtherError;
            }
        }
    }
}
