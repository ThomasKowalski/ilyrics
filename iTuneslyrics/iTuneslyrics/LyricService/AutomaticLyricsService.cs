﻿using iLyrics.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iLyrics.LyricService
{
    class AutomaticLyricsService : ILyricService
    {
        AZLyrics AZLyricsClient;
        Genius GeniusClient;
        //LyricWiki LyricWikiClient;

        private List<ILyricService> ClientsList;

        // lastUsed is used in order to find what service found lyrics for a song
        private string lastUsed = "Genius";

        public string GetName()
        {
            return "Automatic (" + lastUsed + ")";
        }


        public AutomaticLyricsService(Dictionary<String, List<int>> dictionaryIgnore)
        {
            AZLyricsClient = new AZLyrics(dictionaryIgnore.ContainsKey("azlyrics") ? dictionaryIgnore["azlyrics"] : new List<int>());
            GeniusClient = new Genius(dictionaryIgnore.ContainsKey("genius") ? dictionaryIgnore["genius"] : new List<int>());

            //Gone until I create one:
            //LyricWikiClient = new LyricWiki(dictionaryIgnore["lyricwiki"]);

            ClientsList = new List<ILyricService>();
            ClientsList.Add(GeniusClient);
            //ClientsList.Add(LyricWikiClient);
            ClientsList.Add(AZLyricsClient);
        }

        public SearchResult Exists(string artist, string song, out string url)
        {
            foreach (ILyricService Service in ClientsList)
            {
                // Kind reminder: lastUsed is used to determine what service found the URL
                lastUsed = Service.GetName();

                // Verify whether or not a service has lyrics for the song
                SearchResult result = Service.Exists(artist, song, out url);

                if (result == SearchResult.Found)
                    return SearchResult.Found;
                else
                    //Before trying the next service, we verify that the problem doesn't come from an invalid Genius API Token.
                    if (Service.GetName() == "Genius")
                    if (result == SearchResult.InvalidKey)
                        return result;
            }
            url = null;
            return SearchResult.NotFound;
        }

        public SearchResult GetLyrics(string artist, string song, out string lyrics, string url = null)
        {
            string lyricsTest = "";
            foreach (ILyricService Service in ClientsList)
            {
                // Loop through the services until we find the one that generated the URL
                if (!Service.GetName().Equals(lastUsed))
                    continue;

                // Get the lyrics, outputting them to lyricsTest
                Service.GetLyrics(artist, song, out lyricsTest, url);

                // If not empty, update the lyrics and replace Found (it's an old thing, related to the fact that, before, we just had GetLyrics)
                if (lyricsTest != "")
                {
                    lyrics = lyricsTest;
                    return SearchResult.Found;
                }
            }

            // If we can't find the service that generated the URL (which shouldn't happen, but you never know), make lyrics be "" and return NotFound
            lyrics = "";
            return SearchResult.NotFound;
        }
    }
}
