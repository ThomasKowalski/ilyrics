﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using iLyrics.com.wikia.lyrics;
using iLyrics.Interface;
using System.Collections.Generic;
using HtmlAgilityPack;

namespace iLyrics.LyricService
{
    public class LyricWikiReturns : ILyricService
    {
        private List<int> _listIgnore;
        private bool ignore = false;

        public string GetName()
        {
            return "LyricWiki";
        }

        public LyricWikiReturns(List<int> listIgnore)
        {
            if (listIgnore == null)
            {
                ignore = true;
                _listIgnore = null;
            }
            else
                _listIgnore = listIgnore;
        }

        string goodName = "";

        public SearchResult Exists(string artist, string song, out string url)
        {
            List<String> Names = Alternatives(artist, song);
            WebClient MainWC = new WebClient();

            foreach (string songAlt in Names)
            {
                string result = MainWC.DownloadString("http://lyrics.wikia.com/wiki/" + songAlt);
                HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.LoadHtml(result);
                foreach (var div in htmlDoc.DocumentNode.Descendants("div"))
                {
                    try
                    {
                        if (div.Attributes["class"].Value == "lyricbox")
                        {
                            goodName = songAlt;
                            url = "http://lyrics.wikia.com/wiki/" + songAlt;
                            return SearchResult.Found;
                        }
                    }
                    catch (Exception)
                    {
                        //The current div doesn't have any class attributex
                    }
                }
            }
            url = null;
            return SearchResult.Found;

        }

        //Generates a couple of "aliases" : other song URLs that may work (removes useless data in the title, the featuring...) 
        private List<string> Alternatives(string artist, string song)
        {
            string oSong = song; //Original info
            string oArtist = artist;

            List<string> returned = new List<string>();
            returned.Add(artist + ":" + song);

            List<String> symbols = new List<String>();
            symbols.Add("(,)");
            symbols.Add("[,]");
            symbols.Add("{,}");
            foreach (string symbolCouple in symbols)
            {
                string symbol1 = symbolCouple.Split(',')[0];
                string symbol2 = symbolCouple.Split(',')[1];
                var parenStartIndex = song.IndexOf(symbol1);
                var parenEndIndex = song.IndexOf(symbol2);

                if (parenStartIndex > -1 && parenEndIndex > -1)
                {
                    song = (oSong.Remove(parenStartIndex, parenEndIndex - parenStartIndex + 1)).Trim();
                }
            }

            returned.Add(oArtist + ":" + song);
            song = oSong; 

            foreach (string symbolCouple in symbols)
            {
                string symbol1 = symbolCouple.Split(',')[0];
                string symbol2 = symbolCouple.Split(',')[1];
                var parenStartIndex = song.IndexOf(symbol1);
                var parenEndIndex = song.IndexOf(symbol2);

                if (parenStartIndex > -1 && parenEndIndex > -1)
                {
                    artist = (oArtist.Remove(parenStartIndex, parenEndIndex - parenStartIndex + 1)).Trim();
                }
            }

            returned.Add(artist + ":" + oSong);
            artist = oArtist;

            foreach (string symbolCouple in symbols)
            {
                string symbol1 = symbolCouple.Split(',')[0];
                string symbol2 = symbolCouple.Split(',')[1];
                var parenStartIndex = song.IndexOf(symbol1);
                var parenEndIndex = song.IndexOf(symbol2);

                if (parenStartIndex > -1 && parenEndIndex > -1)
                {
                    artist = (oArtist.Remove(parenStartIndex, parenEndIndex - parenStartIndex + 1)).Trim();
                    song = (oSong.Remove(parenStartIndex, parenEndIndex - parenStartIndex + 1)).Trim();
                }
            }

            returned.Add(artist + ":" + song);

            for (int i = 0; i < returned.Count; i++)
            {
                returned[i] = returned[i].Replace(" ", "_");
                returned[i] = System.Uri.EscapeUriString(returned[i]);
            }

            return returned;
        }

        public SearchResult GetLyrics(string artist, string song, out string lyrics, string url = null)
        {
            lyrics = string.Empty;

            if (goodName == "")
                Exists(artist, song, out url);

            if (goodName == "")
                return SearchResult.NotFound;

            if (!ignore && _listIgnore.IndexOf(artist.GetHashCode() * song.GetHashCode()) != -1)
                return SearchResult.NotFound;

            WebClient MainWC = new WebClient();

            string result = MainWC.DownloadString("http://lyrics.wikia.com/wiki/" + goodName);
            HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(result);
            foreach (var div in htmlDoc.DocumentNode.Descendants("div"))
            {
                try
                {
                    if (div.Attributes["class"].Value == "lyricbox")
                    {
                        //Replace all line break tags by C# line breaks
                        div.InnerHtml = div.InnerHtml.Replace("<br />", "\n");
                        div.InnerHtml = div.InnerHtml.Replace("<br/>", "\n");
                        div.InnerHtml = div.InnerHtml.Replace("<br>", "\n");

                        //Remove comments
                        var nodes = htmlDoc.DocumentNode.SelectNodes("//comment()");
                        if (nodes != null)
                        {
                            foreach (HtmlNode comment in nodes)
                            {
                                comment.ParentNode.RemoveChild(comment);
                            }
                        }

                        lyrics = HttpUtility.HtmlDecode(div.InnerText).Trim(); //Decode the HTML Entities, Remove any Space before / after lyrics
                        return SearchResult.Found;
                    }
                }
                catch (Exception)
                {
                    //The current div doesn't have any class attribute
                }
            }
            return SearchResult.Error;
        }
    }
}