﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace iLyrics
{
    static class Others
    {
        public static void writeIgnoreList(string serviceName, List<int> listIgnore)
        {
            StreamWriter writer = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\data-" + serviceName + ".dat", false);
            string whatToWrite = "";
            foreach (int ignorableTrack in listIgnore)
            {
                whatToWrite += ignorableTrack + ";";
            }
            whatToWrite = whatToWrite.Substring(0, whatToWrite.Length - 1);
            writer.Write(whatToWrite);
            writer.Close();
        }
    }
}
