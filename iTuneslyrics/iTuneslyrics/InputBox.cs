﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace iLyrics
{
    public partial class InputBox : Form
    {
        public string userString;
        public Boolean _canBeEmpty = false;
        private System.Windows.Forms.Form _parent;

        public InputBox(System.Windows.Forms.Form parent, Boolean canBeEmpty)
        {
            InitializeComponent();
            this._parent = parent;
            this._canBeEmpty = canBeEmpty;

            bValidate.Enabled = canBeEmpty;
        }

        private void bValidate_Click(object sender, EventArgs e)
        {
            userString = inputText.Text;
            this.Close();
        }

        private void inputText_TextChanged(object sender, EventArgs e)
        {
            if (!this._canBeEmpty && inputText.Text == "")
                bValidate.Enabled = false;
            else
                bValidate.Enabled = true;
            }

        private void InputBox_Load(object sender, EventArgs e)
        {

        }
    }
}
