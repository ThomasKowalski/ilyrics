﻿namespace iLyrics
{
    partial class iLyricsConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(iLyricsConsole));
            this.tbInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.resetConsoleLink = new System.Windows.Forms.LinkLabel();
            this.tbOutput = new System.Windows.Forms.TextBox();
            this.bValidate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbInput
            // 
            this.tbInput.Location = new System.Drawing.Point(12, 26);
            this.tbInput.Name = "tbInput";
            this.tbInput.Size = new System.Drawing.Size(460, 20);
            this.tbInput.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Input:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Output:";
            // 
            // resetConsoleLink
            // 
            this.resetConsoleLink.AutoSize = true;
            this.resetConsoleLink.Location = new System.Drawing.Point(60, 64);
            this.resetConsoleLink.Name = "resetConsoleLink";
            this.resetConsoleLink.Size = new System.Drawing.Size(36, 13);
            this.resetConsoleLink.TabIndex = 3;
            this.resetConsoleLink.TabStop = true;
            this.resetConsoleLink.Text = "(clear)";
            this.resetConsoleLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.resetConsoleLink_Clicked);
            // 
            // tbOutput
            // 
            this.tbOutput.Location = new System.Drawing.Point(12, 80);
            this.tbOutput.Multiline = true;
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.ReadOnly = true;
            this.tbOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbOutput.Size = new System.Drawing.Size(460, 183);
            this.tbOutput.TabIndex = 4;
            this.tbOutput.Text = "Yo.\r\nPlease enter commands. To validate, press enter.";
            // 
            // bValidate
            // 
            this.bValidate.Location = new System.Drawing.Point(397, 51);
            this.bValidate.Name = "bValidate";
            this.bValidate.Size = new System.Drawing.Size(75, 23);
            this.bValidate.TabIndex = 5;
            this.bValidate.Text = "Validate";
            this.bValidate.UseVisualStyleBackColor = true;
            this.bValidate.Click += new System.EventHandler(this.bValidate_Click);
            // 
            // iLyricsConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 279);
            this.Controls.Add(this.bValidate);
            this.Controls.Add(this.tbOutput);
            this.Controls.Add(this.resetConsoleLink);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbInput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "iLyricsConsole";
            this.Text = "Console";
            this.Load += new System.EventHandler(this.iLyricsConsole_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel resetConsoleLink;
        private System.Windows.Forms.TextBox tbOutput;
        private System.Windows.Forms.Button bValidate;

    }
}