﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace iLyrics
{
    public partial class Settings : Form
    {
        private new SettingsManager MainSettings;
        public Settings()
        {
            InitializeComponent();
        }

        private void bClearCache_Click(object sender, EventArgs e)
        {
            try
            {
                System.IO.Directory.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\", true);
                System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\");
                MessageBox.Show("Album artwork and not found lyrics cache was cleared!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Print(ex.Message);
            }
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            MainSettings = new SettingsManager();
            if (MainSettings.CX != SettingsManager.defaultCX) 
                tbCX.Text = MainSettings.CX;
            if (MainSettings.GeniusAPIKey != SettingsManager.defaultGeniusKey)
                tbGeniusAPIKey.Text = MainSettings.GeniusAPIKey;
            if (MainSettings.GoogleAPIKey != SettingsManager.defaultGoogleKey)
                tbGoogleAPIKey.Text = MainSettings.GoogleAPIKey;
        }

        private void lnkGeniusAPIKey_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           DialogResult OpenBrowser = MessageBox.Show("If you want, you can use your own Genius API Key. Do you want to get it now?\n(Choosing 'Yes' will open your Web browser.)", "Genius API Key", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
           if (OpenBrowser == DialogResult.No)
               return;
           Process.Start("https://bitbucket.org/ThomasKowalski/ilyrics/wiki/Create%20a%20Genius%20app%20to%20get%20lyrics");
        }

        private void lnkCX_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogResult OpenBrowser = MessageBox.Show("If you want, you can use your own Google Search Engine. Do you want to create it now?\n(Choosing 'Yes' will open your Web browser.)", "Genius API Key", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (OpenBrowser == DialogResult.No)
                return;
            Process.Start("https://bitbucket.org/ThomasKowalski/ilyrics/wiki/Create%20a%20custom%20Google%20Search%20Engine");
        }

        private void lnkGoogleAPIKey_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogResult OpenBrowser = MessageBox.Show("If you use your own Google Search Engine, you need to enter your Google API Key. Do you want to learn how to get it?\n(Choosing 'Yes' will open your Web browser.)", "Genius API Key", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (OpenBrowser == DialogResult.No)
                return;
            Process.Start("https://bitbucket.org/ThomasKowalski/ilyrics/wiki/Get%20your%20Google%20API%20Key");

        }

        private void bSave_Click(object sender, EventArgs e)
        {
            MainSettings.CX = tbCX.Text;
            MainSettings.GoogleAPIKey = tbGoogleAPIKey.Text;
            MainSettings.GeniusAPIKey = tbGeniusAPIKey.Text;
            this.Close();
        }
    }
}
