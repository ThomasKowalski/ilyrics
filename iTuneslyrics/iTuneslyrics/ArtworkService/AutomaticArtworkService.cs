﻿using iLyrics.Interface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace iLyrics.ArtworkService
{
    public class AutomaticArtworkService : IArtworkService
    {
        private Deezer _deezerService;
        private AllCDCovers _allCDCoversService;

        private string lastUsed = "Deezer";

        public string GetName()
        {
            return lastUsed;
        }

        public AutomaticArtworkService()
        {
            _deezerService = new Deezer();
            _allCDCoversService = new AllCDCovers();
        }

        public Image findCover(string artist, string song, string album)
        {
            return null;
        }

        public Boolean fetchArtwork(string artist, string album)
        {
            if (_deezerService.fetchArtwork(artist, album))
                return true;
            else
            {
                lastUsed = "AllCDCovers";
                if (_allCDCoversService.fetchArtwork(artist, album))
                    return true;
                else
                    return false;
            }
        }
    }
}
