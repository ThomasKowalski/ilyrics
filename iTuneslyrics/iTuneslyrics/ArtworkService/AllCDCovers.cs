﻿/*
 * AllCDCovers Art Finder class by Thomas Kowalski
 * Using allcdcovers.com resources
 * Please include a link to https://bitbucket.org/ThomasKowalski/ilyrics and to http://thomaskowalski.net if you use it
 * For any help, send me an e-mail to thomaskowalski [at] outlook [com]
 * And it's not authorized for commercial use. Sorry.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Drawing;
using iLyrics.Interface;
using iTunesLib;

namespace iLyrics
{
    public class AllCDCovers : IArtworkService
    {
        WebClient generalWebClient;

        public AllCDCovers()
        {
            generalWebClient = new WebClient();
            if (!System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics"))
                System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics");
        }

        public string GetName()
        {
            return "AllCDCovers";
        }

        public Image findCover(string artist, string song, string album)
        {
            string searchURL = "https://www.allcdcovers.com/search/all/all/";
            string webPageContent = "";
            try
            {
                webPageContent = generalWebClient.DownloadString(searchURL + artist + " " + album);
            }
            catch (Exception)
            {
                return null;
            }

            webPageContent = webPageContent.Replace("\n", "");

            string nextURL = getBetweenTags("<img src=\"/images/dear_heavens_not_this_again.gif\" width=\"1\" height=\"19\"/></a></td><td><a href=\"/", "\">Front</a>", webPageContent);

            webPageContent = generalWebClient.DownloadString("http://www.allcdcovers.com/" + nextURL);

            webPageContent = webPageContent.Replace("\n", "");

            nextURL = getBetweenTags("<h2 id=\"h2coverinfo\">Cover Info:</h2>", "<img alt=\"Download cover\" src=\"/images/en/download.gif\"/>", webPageContent);
            nextURL = getToEnd("<a href=\"", nextURL);
            nextURL = nextURL.Substring(0, nextURL.Length - 2); //Removing the end of the html tag

            System.Diagnostics.Debug.Print(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            try
            {
                generalWebClient.DownloadFile("https://www.allcdcovers.com/" + nextURL, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\" + artist + " " + album + ".jpg");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Print(ex.Message);
            }

            Image returned = Image.FromFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\" + artist + " " + album + ".jpg");
            returned.Dispose();
            return returned;
        }

        public Boolean fetchArtwork(string artist, string album)
        {
            if (artist == "" || artist == null)
                return false;
            if (System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\" + artist + " " + album + ".jpg"))
                return true; //If we have already downloaded this artwork, we have already stored it, so we don't want to download it again!
            else
            {
                Image myFoundArt = findCover(artist, "", album);
                if (myFoundArt != null)
                    return true;
                else
                    return fetchArtwork(artist, "");
            }
        }

        private string applyHtmlElements(string text)
        {
            text = text.Replace("&quot;", "\"");
            text = text.Replace("&amt;", "&");
            text = text.Replace("&lt;", "<");
            text = text.Replace("&gt;", ">");
            return text;
        }

        private bool containsHtmlTagging(string text)
        {
            text = text.ToLower();
            List<string> tags = new List<string>(new string[] { "<i>", "</i>", "<br>", "<div>", "</div>", "<br />", "<br/>", "<b>", "</b>" });
            foreach (string tag in tags)
            {
                if (text.Contains(tag))
                    return true;
            }
            return false;
        }

        private string removeHtmlTagging(string text)
        {
            List<string> tags = new List<string>(new string[] { "<i>", "</i>", "<br>", "<div>", "</div>", "<br />", "<br/>", "<b>", "</b>" });
            foreach (string tag in tags)
            {
                text = text.Replace(tag, "");
            }
            return text;
        }

        private string normalize(string toNormalize)
        {
            if (toNormalize.Contains(" ft."))
                toNormalize = getToEnd(" ft.", toNormalize);
            if (toNormalize.Contains(" feat."))
                toNormalize = getToEnd(" feat.", toNormalize);
            if (toNormalize.Contains(" feat"))
                toNormalize = getToEnd(" feat", toNormalize);
            if (toNormalize.Contains(" ft"))
                toNormalize = getToEnd(" ft", toNormalize);
            if (toNormalize.Contains(" featuring"))
                toNormalize = getToEnd(" featuring", toNormalize);

            toNormalize = toNormalize.Replace(" ", "");
            toNormalize = toNormalize.Replace("$", "");

            return toNormalize.ToLower();
        }

        private string getBetweenTags(string tag1, string tag2, string text)
        {
            //Second delimiting word
            int nIndexStart = text.IndexOf(tag1);
            //Find the first occurrence of f1
            int nIndexEnd = text.IndexOf(tag2);
            //Find the first occurrence of f2

            //-1 means the word was not found.
            if (nIndexStart > -1 && nIndexEnd > -1)
            {
                string res = text.Substring(nIndexStart + tag1.Length, nIndexEnd - nIndexStart - tag1.Length);
                //Crop the text between
                return (res);
                //Display
            }
            else
            {
                return ("One or both of the delimiting words were not found!");
            }
        }

        private string getToTag(string tag1, string text)
        {
            //Second delimiting word
            int nIndexStart = text.IndexOf(tag1);
            //Find the first occurrence of f1

            //-1 means the word was not found.
            if (nIndexStart > -1)
            {
                string res = text.Substring(0, nIndexStart);
                //Crop the text between
                return (res);
                //Display
            }
            else
            {
                return ("One or both of the delimiting words were not found!");
            }
        }
        private string getToEnd(string tag1, string text)
        {
            //Second delimiting word
            int nIndexStart = text.IndexOf(tag1);
            //Find the first occurrence of f1

            //-1 means the word was not found.
            if (nIndexStart > -1)
            {
                string res = text.Substring(nIndexStart + tag1.Length, text.Length - nIndexStart - tag1.Length);
                //Crop the text between
                return (res);
                //Display
            }
            else
            {
                return ("One or both of the delimiting words were not found!");
            }
        }
    }
}
