﻿/*
 * Deezer Album Art Finder class by Thomas Kowalski
 * Using deezer.com resources and Google Custom Search
 * Please include a link to https://bitbucket.org/ThomasKowalski/ilyrics and to http://thomaskowalski.net if you use it
 * For any help, send me an e-mail to thomaskowalski [at] outlook [com]
 * And it's not authorized for commercial use. Sorry.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Drawing;
using iLyrics.Interface;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using iTunesLib;

namespace iLyrics
{
    class Deezer : IArtworkService
    {
        WebClient generalWebClient;

        public Deezer()
        {
            generalWebClient = new WebClient();
            if (!System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics"))
                System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics");
        }

        public string GetName()
        {
            return "Deezer";
        }

        public Image findCover(string artist, string song, string album)
        {
            string searchURL = "https://www.googleapis.com/customsearch/v1?searchtype=image&key=" + (new SettingsManager()).GoogleAPIKey + "&cx=" + (new SettingsManager()).CX + "&q=";
            string webPageContent = "";
            try
            {
                webPageContent = generalWebClient.DownloadString(searchURL + Uri.EscapeUriString(artist + " " + album));
            }
            catch (Exception)
            {
               
            }

            Newtonsoft.Json.Linq.JObject response = JObject.Parse(webPageContent);
            string url = response["items"][0]["pagemap"]["cse_image"][0]["src"].ToString().Replace("500x500", "1000x1000");

            try
            {
                generalWebClient.DownloadFile(url, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\" + artist + " " + album + ".jpg");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Print(ex.Message);
            }


            Image returned = Image.FromFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\" + artist + " " + album + ".jpg");
            returned.Dispose();
            return returned;
        }

        public Boolean fetchArtwork(string artist, string album)
        {
            if (artist == "" || artist == null)
                return false;
            if (System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\" + artist + " " + album + ".jpg"))
                return true; //If we have already downloaded this artwork, we have already stored it, so we don't want to download it again!
            else
            {
                Image myFoundArt = findCover(artist, "", album);
                if (myFoundArt != null)
                    return true;
                else
                    return fetchArtwork(artist, "");
            }
        }
    }
}
