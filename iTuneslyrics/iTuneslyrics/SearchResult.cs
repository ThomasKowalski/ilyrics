namespace iLyrics
{
    public enum SearchResult
    {
        Found,
        NotFound,
        MultipleFound,
        Error,
        InvalidKey,
        OtherError
    }
}