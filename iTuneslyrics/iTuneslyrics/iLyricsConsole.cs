﻿using iTunesLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace iLyrics
{
    public partial class iLyricsConsole : Form
    {
        private iTunesLib.iTunesAppClass _iTunesApp;
        private List<string> commands;
        private Dictionary<string, List<string>> arguments;

        private long numTracks;
        private IITLibraryPlaylist mainLibrary;
        private IITSource mainLibrarySource;
        private IITTrackCollection tracks;

        public iLyricsConsole()
        {
            InitializeComponent();
        }

        private void iLyricsConsole_Load(object sender, EventArgs e)
        {
            _iTunesApp = new iTunesAppClass();
            _iTunesApp.BrowserWindow.Visible = true;
            _iTunesApp.BrowserWindow.Minimized = false;
            this.AcceptButton = bValidate;
            bValidate.Width = 0;
            commands = new List<string>();
            arguments = new Dictionary<string, List<string>>();
            commands.Add("createplaylist");
            commands.Add("average");
            commands.Add("itunesfriendfilter");
            commands.Add("createhaslyricsplaylist");
            arguments.Add("createplaylist", new List<string>());
        }

        setToScan currentSet;
        private enum setToScan
        {
            wholeLibrary,
            selectedItems,
            cancelAction,
        }


        private void chooseSetToScan()
        {
            DialogResult onlySelectedTracks;
            try
            {
                var testException = _iTunesApp.SelectedTracks.Count > 0;
                onlySelectedTracks = MessageBox.Show("Do you want to scan only the selected songs?", "Scan options", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            }
            catch (Exception)
            {
                onlySelectedTracks = DialogResult.No;
            }
            switch (onlySelectedTracks)
            {
                case DialogResult.Yes:
                    currentSet = setToScan.selectedItems;
                    break;
                case DialogResult.No:
                    currentSet = setToScan.wholeLibrary;
                    break;
                case DialogResult.Cancel:
                    currentSet = setToScan.cancelAction;
                    break;
            }
        }


        private void bValidate_Click(object sender, EventArgs e)
        {
            string[] command = tbInput.Text.Split(' ');
            echo(tbInput.Text);
            tbInput.Text = "";
            if (commands.IndexOf(command[0]) == -1)
                echo("Invalid command. Please try again.");
            else
            {
                mainLibrary = _iTunesApp.LibraryPlaylist;
                mainLibrarySource = _iTunesApp.LibrarySource;
                tracks = mainLibrary.Tracks;
                numTracks = tracks.Count;


                if (command[0] == "average")
                {
                    determineAverage();
                }
                else if (command[0] == "createplaylist")
                {
                    string criterion = "";
                    string searchedWord = "";
                    int quantity = 0;
                    string unit = "";
                    for (int i = 1; i <= command.Length - 1; i++)
                    {
                        // Split the rest of the string
                        string[] currentArgument = command[i].Split(':');
                        // currentArgument[0] is the paramter name, followed by a semicolon and then the value of the parameter
                        if (currentArgument[0] == "criterion")
                            criterion = currentArgument[1];

                        else if (currentArgument[0] == "word")
                            searchedWord = currentArgument[1];

                        else if (currentArgument[0] == "quantity")
                            quantity = int.Parse(currentArgument[1]);

                        else if (currentArgument[0] == "unit")
                            unit = currentArgument[1];
                    }
                    createPlaylist(criterion, searchedWord, quantity, unit);
                    tbInput.Enabled = true;
                }
                else if (command[0] == "createhaslyricsplaylist")
                {
                    chooseSetToScan();
                    if (currentSet == setToScan.cancelAction)
                        return; //exit function if the user clicked Cancel

                    if (currentSet == setToScan.selectedItems)
                    {
                        tracks = _iTunesApp.SelectedTracks;
                        numTracks = tracks.Count;
                    }

                    tbInput.Enabled = false;
                    createHasLyricsPlaylist();
                    tbInput.Enabled = true;
                }
                else if (command[0] == "itunesfriendfilter")
                {
                    iTunesFriendFilter();
                }

            }
        }

        private void echo(string s)
        {
            
            tbOutput.Text += Environment.NewLine + s;
        }

        private void resetConsoleLink_Clicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tbOutput.Text = "";
        }

        #region "Functions"
        private void createPlaylist(string criterion, string searchedWord, int quantity, string unit)
        {
            echo("Started creating playlist. It can take a long time, please be patient ; do not close this window.");
            echo("Tracks to analyze: " + numTracks);
            tbInput.Enabled = false;

            iTunesLib.IITPlaylist createdPlaylist;
            createdPlaylist = _iTunesApp.CreatePlaylist("Console created (" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + ")");

            for (var i = 1; i <= numTracks; i++)
            {
                IITTrack currTrack = tracks[i] as IITFileOrCDTrack;
                try
                {
                    string lyrics = ((IITFileOrCDTrack)currTrack).Lyrics;
                    if (lyrics != null && ((IITFileOrCDTrack)currTrack).VideoKind == ITVideoKind.ITVideoKindNone && ((IITFileOrCDTrack)currTrack).Podcast == false)
                    {
                        // For "word is contained" criterion
                        if (criterion == "word" && lyrics.IndexOf(searchedWord) != -1)
                            ((IITUserPlaylist)createdPlaylist).AddTrack(currTrack);

                        // WORDS RELATED CRITERIA
                        // For number of words > n
                        if (criterion == ">" && unit == "w" && lyrics.Split(' ').Length > quantity)
                            ((IITUserPlaylist)createdPlaylist).AddTrack(currTrack);

                        // For number of words < 
                        else if (criterion == "<" && unit == "w" && lyrics.Split(' ').Length < quantity)
                            ((IITUserPlaylist)createdPlaylist).AddTrack(currTrack);

                        // For number of words =
                        else if (criterion == "=" && unit == "w" && lyrics.Length == quantity)
                            ((IITUserPlaylist)createdPlaylist).AddTrack(currTrack);

                        // CHARS RELATED CRITERIA
                        // For number of chars > 
                        else if (criterion == ">" && unit == "c" && lyrics.Length > quantity)
                            ((IITUserPlaylist)createdPlaylist).AddTrack(currTrack);

                        // For number of chars <
                        else if (criterion == "<" && unit == "c" && lyrics.Length < quantity)
                            ((IITUserPlaylist)createdPlaylist).AddTrack(currTrack);

                        // For number of chars = 
                        else if (criterion == "=" && unit == "c" && lyrics.Length == quantity)
                            ((IITUserPlaylist)createdPlaylist).AddTrack(currTrack);
                    }
                }
                catch (Exception ex)
                {
                    //echo(ex.Message);
                }

            }
            echo("Done!");

        }

        private void determineAverage()
        {
            int tracksScanned = 0;
            long totalCharacters = 0;
            for (var i = 1; i <= numTracks; i++)
            {
                IITTrack currTrack = tracks[i] as IITFileOrCDTrack;
                try
                {
                    string lyrics = ((IITFileOrCDTrack)currTrack).Lyrics;
                    if (lyrics != null && ((IITFileOrCDTrack)currTrack).VideoKind == ITVideoKind.ITVideoKindNone && ((IITFileOrCDTrack)currTrack).Podcast == false)
                    {
                        tracksScanned += 1;
                        totalCharacters += lyrics.Length;
                    }
                }
                catch (Exception ex)
                {
                    //echo(ex.Message);
                }
            }
            echo("Done! Average: " + totalCharacters / tracksScanned);
        }

        private void createHasLyricsPlaylist()
        {
            echo("Started creating playlist. It can take a long time, please be patient ; do not close this window.");
            echo("Tracks to analyze: " + numTracks);

            iTunesLib.IITPlaylist createdPlaylist;
            createdPlaylist = _iTunesApp.CreatePlaylist("Songs with lyrics (" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + ")");

            for (var i = 1; i <= numTracks; i++)
            {
                IITTrack currTrack = tracks[i] as IITFileOrCDTrack;
                try
                {
                    string lyrics = ((IITFileOrCDTrack)currTrack).Lyrics;
                    if (lyrics != null && lyrics != "")
                    {
                        ((IITUserPlaylist)createdPlaylist).AddTrack(currTrack);
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Print("Exception in createHasLyricsPlaylist:\n" + ex.Message);
                }

            }
            echo("Done!");
        }

        private void iTunesFriendFilter()
        {
            for (var i = 1; i <= numTracks; i++)
            {
                IITTrack currTrack = tracks[i] as IITFileOrCDTrack;
                try
                {
                    string lyrics = ((IITFileOrCDTrack)currTrack).Lyrics;
                    if (lyrics != null && ((IITFileOrCDTrack)currTrack).VideoKind == ITVideoKind.ITVideoKindNone && ((IITFileOrCDTrack)currTrack).Podcast == false)
                    {
                        if (lyrics.Contains("+-+-+-+-+-+--+--+-+-+-+-+-+-+-+-+-+"))
                        {
                            string pattern = "((((\\+-)+)(\\+--\\+--)((\\+-)+)\\+)\\r\\n)(.*\\r\\n)*"; //We use a Regex to get all the text that starts with the +-... 
                                                                                                       //((\\+-)+)     : at least once "+-"
                                                                                                       //(\\+--\\+--)  : two times exactly "+--"
                                                                                                       //((\\+-)+)\\+) : once again at least once "+-" and after "+" once
                                                                                                       //\\r\\n        : line feed
                                                                                                       //(.*\\r\\n)*   : any string and a line feed as many times as possible
                            Regex rgx = new Regex(pattern);
                            ((IITFileOrCDTrack)currTrack).Lyrics = rgx.Replace(lyrics, "").Replace("+-+-+-+-+-+--+--+-+-+-+-+-+-+-+-+-+", "").Trim();  //And we replace it with a blank string (and we replace the +-... one more time) (and we Trim() it because it always looks better!)
                        }
                    }
                }
                catch (Exception ex)
                {
                    //echo(ex.Message);
                }
            }
            echo("Done!");
        }

        #endregion
    }
}