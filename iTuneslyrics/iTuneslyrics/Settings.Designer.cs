﻿namespace iLyrics
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.bSave = new System.Windows.Forms.Button();
            this.bClearCache = new System.Windows.Forms.Button();
            this.lblGeniusAPI = new System.Windows.Forms.Label();
            this.tbGeniusAPIKey = new System.Windows.Forms.TextBox();
            this.lblCX = new System.Windows.Forms.Label();
            this.tbCX = new System.Windows.Forms.TextBox();
            this.lblGoogleAPIKey = new System.Windows.Forms.Label();
            this.tbGoogleAPIKey = new System.Windows.Forms.TextBox();
            this.lnkGeniusAPIKey = new System.Windows.Forms.LinkLabel();
            this.lnkCX = new System.Windows.Forms.LinkLabel();
            this.lnkGoogleAPIKey = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // bSave
            // 
            this.bSave.Location = new System.Drawing.Point(254, 160);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 23);
            this.bSave.TabIndex = 0;
            this.bSave.Text = "Save";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // bClearCache
            // 
            this.bClearCache.Location = new System.Drawing.Point(12, 160);
            this.bClearCache.Name = "bClearCache";
            this.bClearCache.Size = new System.Drawing.Size(119, 23);
            this.bClearCache.TabIndex = 1;
            this.bClearCache.Text = "Clear cache";
            this.bClearCache.UseVisualStyleBackColor = true;
            this.bClearCache.Click += new System.EventHandler(this.bClearCache_Click);
            // 
            // lblGeniusAPI
            // 
            this.lblGeniusAPI.AutoSize = true;
            this.lblGeniusAPI.Location = new System.Drawing.Point(9, 9);
            this.lblGeniusAPI.Name = "lblGeniusAPI";
            this.lblGeniusAPI.Size = new System.Drawing.Size(84, 13);
            this.lblGeniusAPI.TabIndex = 2;
            this.lblGeniusAPI.Text = "Genius API Key:";
            // 
            // tbGeniusAPIKey
            // 
            this.tbGeniusAPIKey.Location = new System.Drawing.Point(12, 25);
            this.tbGeniusAPIKey.Name = "tbGeniusAPIKey";
            this.tbGeniusAPIKey.Size = new System.Drawing.Size(317, 20);
            this.tbGeniusAPIKey.TabIndex = 3;
            // 
            // lblCX
            // 
            this.lblCX.AutoSize = true;
            this.lblCX.Location = new System.Drawing.Point(9, 53);
            this.lblCX.Name = "lblCX";
            this.lblCX.Size = new System.Drawing.Size(136, 13);
            this.lblCX.TabIndex = 2;
            this.lblCX.Text = "Google Custom Search CX:";
            // 
            // tbCX
            // 
            this.tbCX.Location = new System.Drawing.Point(12, 69);
            this.tbCX.Name = "tbCX";
            this.tbCX.Size = new System.Drawing.Size(317, 20);
            this.tbCX.TabIndex = 3;
            // 
            // lblGoogleAPIKey
            // 
            this.lblGoogleAPIKey.AutoSize = true;
            this.lblGoogleAPIKey.Location = new System.Drawing.Point(9, 97);
            this.lblGoogleAPIKey.Name = "lblGoogleAPIKey";
            this.lblGoogleAPIKey.Size = new System.Drawing.Size(122, 13);
            this.lblGoogleAPIKey.TabIndex = 2;
            this.lblGoogleAPIKey.Text = "Google Search API Key:";
            // 
            // tbGoogleAPIKey
            // 
            this.tbGoogleAPIKey.Location = new System.Drawing.Point(12, 113);
            this.tbGoogleAPIKey.Name = "tbGoogleAPIKey";
            this.tbGoogleAPIKey.Size = new System.Drawing.Size(317, 20);
            this.tbGoogleAPIKey.TabIndex = 3;
            // 
            // lnkGeniusAPIKey
            // 
            this.lnkGeniusAPIKey.AutoSize = true;
            this.lnkGeniusAPIKey.Location = new System.Drawing.Point(316, 9);
            this.lnkGeniusAPIKey.Name = "lnkGeniusAPIKey";
            this.lnkGeniusAPIKey.Size = new System.Drawing.Size(13, 13);
            this.lnkGeniusAPIKey.TabIndex = 4;
            this.lnkGeniusAPIKey.TabStop = true;
            this.lnkGeniusAPIKey.Text = "?";
            this.lnkGeniusAPIKey.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkGeniusAPIKey_LinkClicked);
            // 
            // lnkCX
            // 
            this.lnkCX.AutoSize = true;
            this.lnkCX.Location = new System.Drawing.Point(316, 53);
            this.lnkCX.Name = "lnkCX";
            this.lnkCX.Size = new System.Drawing.Size(13, 13);
            this.lnkCX.TabIndex = 4;
            this.lnkCX.TabStop = true;
            this.lnkCX.Text = "?";
            this.lnkCX.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkCX_LinkClicked);
            // 
            // lnkGoogleAPIKey
            // 
            this.lnkGoogleAPIKey.AutoSize = true;
            this.lnkGoogleAPIKey.Location = new System.Drawing.Point(316, 97);
            this.lnkGoogleAPIKey.Name = "lnkGoogleAPIKey";
            this.lnkGoogleAPIKey.Size = new System.Drawing.Size(13, 13);
            this.lnkGoogleAPIKey.TabIndex = 4;
            this.lnkGoogleAPIKey.TabStop = true;
            this.lnkGoogleAPIKey.Text = "?";
            this.lnkGoogleAPIKey.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkGoogleAPIKey_LinkClicked);
            // 
            // Settings
            // 
            this.AcceptButton = this.bSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 194);
            this.Controls.Add(this.lnkGoogleAPIKey);
            this.Controls.Add(this.lnkCX);
            this.Controls.Add(this.lnkGeniusAPIKey);
            this.Controls.Add(this.tbGoogleAPIKey);
            this.Controls.Add(this.tbCX);
            this.Controls.Add(this.tbGeniusAPIKey);
            this.Controls.Add(this.lblGoogleAPIKey);
            this.Controls.Add(this.lblCX);
            this.Controls.Add(this.lblGeniusAPI);
            this.Controls.Add(this.bClearCache);
            this.Controls.Add(this.bSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Settings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Button bClearCache;
        private System.Windows.Forms.Label lblGeniusAPI;
        private System.Windows.Forms.TextBox tbGeniusAPIKey;
        private System.Windows.Forms.Label lblCX;
        private System.Windows.Forms.TextBox tbCX;
        private System.Windows.Forms.Label lblGoogleAPIKey;
        private System.Windows.Forms.TextBox tbGoogleAPIKey;
        private System.Windows.Forms.LinkLabel lnkGeniusAPIKey;
        private System.Windows.Forms.LinkLabel lnkCX;
        private System.Windows.Forms.LinkLabel lnkGoogleAPIKey;
    }
}