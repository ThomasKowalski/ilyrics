﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iLyrics
{
    class SettingsManager
    {
        #region "Properties"

        public const string defaultGeniusKey = "WVg16wXfgQqLa0Na6zNI2duCNrh8B1RhgojgqVPLuDZNp1GkjAGFP3QqNLHObxaN";
        public const string defaultCX = "016839032996308921873:1pschtkbqye";
        public const string defaultGoogleKey = "AIzaSyBGUtfGZhDuLu7h12Twlzu72F2FdjrQveg";

        public string GeniusAPIKey
        {
            get
            {
                using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(@"Software\\Thomas Kowalski\\iLyrics", true))
                {
                    return (string)registryKey.GetValue("Genius");
                }
            }
            set
            {
                using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(@"Software\\Thomas Kowalski\\iLyrics", true))
                {
                    if (value != "")
                        registryKey.SetValue("Genius", value);
                    else
                        registryKey.SetValue("Genius", defaultGeniusKey);
                }
            }
        }
        public string CX
        {
            get
            {
                using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(@"Software\\Thomas Kowalski\\iLyrics", true))
                {
                    return (string)registryKey.GetValue("CX");
                }
            }
            set
            {
                this.GeniusAPIKey = value;
                using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(@"Software\\Thomas Kowalski\\iLyrics", true))
                {
                    if (value != "")
                        registryKey.SetValue("CX", value);
                    else
                        registryKey.SetValue("CX", defaultCX);
                }
            }
        }
        public string GoogleAPIKey
        {
            get
            {
                using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(@"Software\\Thomas Kowalski\\iLyrics", true))
                {
                    return (string)registryKey.GetValue("Google");
                }
            }
            set
            {
                this.GeniusAPIKey = value;
                using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(@"Software\\Thomas Kowalski\\iLyrics", true))
                {
                    if (value != "")
                        registryKey.SetValue("Google", value);
                    else
                        registryKey.SetValue("Google", defaultGoogleKey);
                }
            }
        }
        #endregion

        public SettingsManager()
        {
            CreateSubkeys();
            DefineDefaultKeys();
        }

        private void DefineDefaultKeys()
        {
            if (GeniusAPIKey == "" || GeniusAPIKey == null)
            {
                GeniusAPIKey = defaultGeniusKey;
            }
            if (GoogleAPIKey == "" || CX == "" || GoogleAPIKey == null || CX == null)
            {
                CX = defaultCX;
                GoogleAPIKey = defaultGoogleKey;
            }
        }
        private void CreateSubkeys()
        {
            try
            {
                RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software", true);
                try
                {
                    registryKey.CreateSubKey("Thomas Kowalski");
                }
                catch (Exception)
                {
                }
                registryKey = Registry.CurrentUser.OpenSubKey("Software\\Thomas Kowalski\\", true);
                registryKey.CreateSubKey("iLyrics");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Print("Exception in SettingsManager: '" + ex.Message + "'");
            }
        }
    }
}
