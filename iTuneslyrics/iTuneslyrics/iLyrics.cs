using System;
using System.Collections.Generic;
using System.Windows.Forms;
using iTunesLib;
using iLyrics.Interface;
using iLyrics.LyricService;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using Microsoft.VisualBasic;
using iLyrics.ArtworkService;

namespace iLyrics
{
    public partial class iLyrics : Form
    {
        IiTunes _iTunesApp;
        ILyricService _lyricService;

        private Boolean testedInternet = false;
        private Boolean internetAccess = true;
        private Boolean newVersion = false;

        private SettingsManager MainSettings;

        public Dictionary<string, List<int>> listIgnore = new Dictionary<string, List<int>>();
        public List<String> listFiles = new List<String>();

        public String inputString = "";


        public iLyrics()
        {
            InitializeComponent();
        }

        public void LoadFiles()
        {
            listFiles.Add("azlyrics");
            listFiles.Add("lyricwiki");
            listFiles.Add("genius");
            if (!System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics"))
            {
                System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics");
            }
            else
            {
                foreach (string serviceFile in listFiles)
                {
                    listIgnore.Add(serviceFile, new List<int>());
                    if (System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\data-" + serviceFile + ".dat"))
                    {
                        StreamReader reader = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\data-" + serviceFile + ".dat", System.Text.Encoding.Default);
                        String[] arrayResults = reader.ReadToEnd().Split(';');
                        reader.Close();
                        foreach (string result in arrayResults)
                        {
                            if (result != "")
                                listIgnore[serviceFile].Add(int.Parse(result));
                        }
                    }
                }

            }
        }

        private void btnGetInfo_Click(object sender, EventArgs e)
        {
            if (testedInternet == false)
            {
                MessageBox.Show("We are currently testing your Internet connection. Please wait for the end of the test before getting lyrics or artworks.", "Testing Internet", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (internetAccess == false)
            {
                MessageBox.Show("iLyrics could not connect to Internet. Please try again later.", "No connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                testedInternet = false;
                checkInternet.RunWorkerAsync();
                return;
            }
            IITTrackCollection selectedTracks = _iTunesApp.SelectedTracks;
            if ((selectedTracks == null))
            {
                MessageBox.Show(null, "You must first load iTunes and select some songs in iTunes.", "No songs selected.", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            IITPlaylist notFoundPlaylist = null;
            IITPlaylist foundPlaylist = null;
            if (selectedTracks.Count > 1)
            {
                if (cbCreatePlaylistFound.Checked)
                {
                    foundPlaylist = _iTunesApp.CreatePlaylist("Found (" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + ")");
                }
                if (cbCreatePlaylistNotFound.Checked)
                {
                    notFoundPlaylist = _iTunesApp.CreatePlaylist("Not found (" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + ")");
                }
            }
            if (chkLyrics.Checked)
            {
                switch (cbService.SelectedItem.ToString())
                {
                    case "(Automatic)":
                        _lyricService = new AutomaticLyricsService(listIgnore);
                        break;
                    case "LyricWiki":
                        //MessageBox.Show(this, "Sorry, but the LyricWiki API has stopped working due to licensing restrictions. You can see more info at " + Environment.NewLine + "http://api.wikia.com/wiki/LyricWiki_API" + Environment.NewLine + "I will try to create a custom made API Client (like I did for Genius and AZLyrics, but it can take some time. For now, just use Genius or AZLyrics, and it'll be back one day.", "Sorry, but... no.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        _lyricService = new LyricWikiReturns(listIgnore["lyricwiki"]);
                        break;
                    case "AZLyrics":
                        _lyricService = new AZLyrics(listIgnore["azlyrics"]);
                        break;
                    case "Genius":
                        _lyricService = new Genius(listIgnore["genius"]);
                        break;
                    default:
                        MessageBox.Show("Please select a lyric service first.");
                        return;
                }
            }
            else
                _lyricService = null;

            IArtworkService _artworkService;
            if (chkAlbumArt.Checked)
            {
                switch (cbArtworkService.SelectedItem.ToString())
                {
                    case "(Automatic)":
                        _artworkService = new AutomaticArtworkService();
                        break;
                    case "Deezer":
                        _artworkService = new Deezer();
                        break;
                    case "AllCDCovers":
                        _artworkService = new AllCDCovers();
                        break;
                    default:
                        MessageBox.Show("Please select an artwork service first.");
                        return;
                }
            }
            else
                _artworkService = null;

            if (chkAuto.Checked)
            {
                FrmResult fr;
                fr = new FrmResult(selectedTracks, _lyricService, chkOverwrite.Checked, cbOverwriteArtwork.Checked, _artworkService, notFoundPlaylist, foundPlaylist);
                fr.ShowDialog();
            }
            else
            {
                int updatedSongsCount = 0;
                for (int i = 1; i <= selectedTracks.Count; i++)
                {
                    var currentTrack = (IITFileOrCDTrack)selectedTracks[i];

                    updatedSongsCount++;
                    ManualUpdate ab = new ManualUpdate
                    {
                        getArtwork = chkAlbumArt.Checked,
                        ArtFinder = _artworkService,
                        overWriteLyrics = chkOverwrite.Checked,
                        overwriteArtwork = cbOverwriteArtwork.Checked,
                        CurrentTrack = currentTrack,
                        LyricService = _lyricService,
                        NotFoundPlaylist = notFoundPlaylist,
                        getLyrics = chkLyrics.Checked
                    };

                    DialogResult dr = ab.ShowDialog();
                    if (dr == DialogResult.Abort)
                    {
                        MessageBox.Show("Update cancelled. Already updated tracks have been saved.", "Operation aborted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                }
                if (updatedSongsCount == 0)
                    MessageBox.Show("All selected songs seem to have lyrics.", "No lyrics to find!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Update completed!", "Done!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void setInput(string input)
        {
            this.inputString = input;
        }

        private void iLyrics_Load(object sender, EventArgs e)
        {
            _iTunesApp = new iTunesAppClass();
            _iTunesApp.BrowserWindow.Visible = true;
            _iTunesApp.BrowserWindow.Minimized = false;
            MainSettings = new SettingsManager();
            cbService.SelectedIndex = 0;
            cbArtworkService.SelectedIndex = 0;
            LoadFiles();
            Layout();
            checkInternet.RunWorkerAsync();
            workerUpdates.RunWorkerCompleted += workerUpdates_RunWorkerCompleted;
            workerUpdatesAuto.RunWorkerAsync();
            workerUpdatesAuto.RunWorkerCompleted += workerUpdatesAuto_RunWorkerCompleted;
            this.FormClosing += iLyrics_FormClosing;
            cbAdvanced_CheckedChanged(null, null);
        }

        private void iTunes()
        {
            bool isStarted = Process.GetProcessesByName("itunes").Length > 0;
            if (!isStarted)
            {
                MessageBox.Show("iTunes is not running and is required to use iLyrics. Please start it and restart iLyrics.", "No iTunes instance detected.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Application.Exit();
            }
        }

        #region "Buttons"
        private void bOpenConsole_Click(object sender, EventArgs e)
        {
            iLyricsConsole console = new iLyricsConsole();
            console.ShowDialog();
        }
        private void bSettings_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            settings.ShowDialog();
        }

        private void bResetArtwork_Click(object sender, EventArgs e)
        {
            MessageBox.Show("I'm sorry, but iLyrics can't do this. But I can tell you how to do it easily.\nFirst, find a random image on your computer. Second, select all the tracks you want to reset artwork for in iTunes.\nRight click on one of them, and click Get Info.\nGo to Artwork, and add the random image you took before.\nClick OK. And then redo the same, but instead of adding the image, click on it and press Delete. Here it is!", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void bGenerateNoLyrics_Click(object sender, EventArgs e)
        {
            chooseSetToScan();
            if (currentSet == setToScan.cancelAction)
                return;
            disableUI();
            currentWork = task.noLyrics;
            workerTasks.RunWorkerAsync();
        }

        private void chooseSetToScan()
        {
            DialogResult onlySelectedTracks;
            try
            {
                var testException = _iTunesApp.SelectedTracks.Count > 0;
                onlySelectedTracks = MessageBox.Show("Do you want to scan only the selected songs?", "Scan options", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            }
            catch (Exception)
            {
                onlySelectedTracks = DialogResult.No;
            }
            switch (onlySelectedTracks)
            {
                case DialogResult.Yes:
                    currentSet = setToScan.selectedItems;
                    break;
                case DialogResult.No:
                    currentSet = setToScan.wholeLibrary;
                    break;
                case DialogResult.Cancel:
                    currentSet = setToScan.cancelAction;
                    break;
            }
        }

        private void bCreateNotFoundOnDiskPlaylist_Click(object sender, EventArgs e)
        {
            chooseSetToScan();
            if (currentSet == setToScan.cancelAction)
                return;
            disableUI();
            currentWork = task.notFoundOnDisk;
            workerTasks.RunWorkerAsync();
        }
        private void bGenerateSkipWhenShuffle_Click(object sender, EventArgs e)
        {
            chooseSetToScan();
            if (currentSet == setToScan.cancelAction)
                return;
            disableUI();
            currentWork = task.skipWhenShuffle;
            workerTasks.RunWorkerAsync();
        }
        private void bGenerateNoArtworkPlaylist_Click(object sender, EventArgs e)
        {
            chooseSetToScan();
            if (currentSet == setToScan.cancelAction)
                return;
            disableUI();
            currentWork = task.noArtwork;
            workerTasks.RunWorkerAsync();
        }

        private void bApplyLyricsToSelected_Click(object sender, EventArgs e)
        {
            if (_iTunesApp.SelectedTracks == null)
            {
                MessageBox.Show(null, "You must first load iTunes and select some songs in iTunes.", "No songs selected.", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            InputBox input = new InputBox(this, false);
            input.ShowDialog();
            disableUI();
            lyricsToSet = input.userString;
            input.Dispose();
            currentWork = task.setLyrics;
            workerTasks.RunWorkerAsync();
        }
        string lyricsToSet = "";

        private void bResetLyrics_Click(object sender, EventArgs e)
        {
            if (_iTunesApp.SelectedTracks == null)
            {
                MessageBox.Show(null, "You must first load iTunes and select some songs in iTunes.", "No songs selected.", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            else
            {
                DialogResult ResetOrNot = MessageBox.Show("Do you really want to remove the lyrics for all the selected tracks?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (ResetOrNot == DialogResult.No)
                    return;
            }
            disableUI();
            currentWork = task.resetLyrics;
            workerTasks.RunWorkerAsync();
        }
        #endregion
        #region "Updates" {
        private void checkInternet_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            WebClient wcTestInternet = new WebClient();
            try
            {
                wcTestInternet.DownloadString("http://perdu.com");
            }
            catch (Exception)
            {
                try
                {
                    wcTestInternet.DownloadString("http://google.com");
                }
                catch (Exception)
                {
                    internetAccess = false;
                    testedInternet = true;
                    return;
                }
            }
            internetAccess = true;
            testedInternet = true;
        }

        private void workerUpdatesAuto_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            while (!internetAccess)
            {
                //Wait for the Internet access.
            }
            if (internetAccess)
            {
                try
                {
                    WebClient updateClient = new WebClient();
                    string lastVersion = updateClient.DownloadString("https://bitbucket.org/ThomasKowalski/ilyrics/raw/master/iTuneslyrics/iTuneslyrics/obj/Debug/version.txt");
                    if (int.Parse(Application.ProductVersion.Replace(".", "")) < int.Parse(lastVersion.Replace(".", "")))
                    {
                        newVersion = true;
                    }
                }
                catch
                {
                    System.Diagnostics.Debug.Print("Couldn't check updates.\nNetwork could be reached but no secure connection to BitBucket servers could be established.");
                }
            }
        }

        private void workerUpdates_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (internetAccess)
            {
                try
                {
                    WebClient updateClient = new WebClient();
                    string lastVersion = updateClient.DownloadString("https://bitbucket.org/ThomasKowalski/ilyrics/raw/master/iTuneslyrics/iTuneslyrics/bin/Debug/version.txt");
                    if (int.Parse(Application.ProductVersion.Replace(".", "")) < int.Parse(lastVersion.Replace(".", "")))
                    {
                        newVersion = true;
                    }
                }
                catch
                {
                    MessageBox.Show("Couldn't check updates.\n" + 
                        "Network could be reached but no secure connection to BitBucket servers could be established.", 
                        "Error", 
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        private void workerUpdatesAuto_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (newVersion)
            {
                DialogResult download = MessageBox.Show("A new version was found! Download it?", 
                    "New version found!", 
                    MessageBoxButtons.YesNo, 
                    MessageBoxIcon.Information);
                if (download == DialogResult.Yes)
                    Process.Start("https://bitbucket.org/ThomasKowalski/ilyrics/raw/master/iTuneslyrics/iTuneslyrics/bin/Debug/iLyrics.zip");
            }
        }


        private void lblCheckUpdates_Click(object sender, EventArgs e)
        {
            workerUpdates.RunWorkerAsync();
        }

        private void workerUpdates_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (testedInternet == false)

                MessageBox.Show("We are currently testing your Internet connection. Please wait for the end of the test before getting lyrics or artworks.", "Testing Internet", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else if (internetAccess == false)
            {
                MessageBox.Show("iLyrics could not connect to Internet. Please try again later.", "No connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                testedInternet = false;
                checkInternet.RunWorkerAsync();
            }
            else if (newVersion)
            {
                DialogResult download = MessageBox.Show("A new version was found! Download it?", "New version found!", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (download == DialogResult.Yes)
                    Process.Start("https://bitbucket.org/ThomasKowalski/ilyrics/raw/master/iTuneslyrics/iTuneslyrics/bin/Debug/iLyrics.zip");
            }
            else
                MessageBox.Show("No new version found.", 
                    "You have the latest version!",
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Information);
        }
        #endregion
        #region "Tasks"

        task currentWork;
        private enum task
        {
            noLyrics,
            noArtwork,
            resetLyrics,
            resetArtwork,
            setLyrics,
            notFoundOnDisk,
            skipWhenShuffle,
        }

        setToScan currentSet;
        private enum setToScan
        {
            wholeLibrary,
            selectedItems,
            cancelAction,
        }

        private void workerTasks_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            iTunesLib.IITPlaylist noLyricsPlaylist;
            var mainLibrary = _iTunesApp.LibraryPlaylist;
            var mainLibrarySource = _iTunesApp.LibrarySource;
            var tracks = mainLibrary.Tracks; //set tracks to the whole library because we can't write "var tracks" without initializing the variable
            if (currentSet == setToScan.selectedItems)
                tracks = _iTunesApp.SelectedTracks;

            var numTracks = tracks.Count;
            this.BeginInvoke((Action)delegate()
            {
                pbProgress.Value = 0;
                if (currentWork == task.resetLyrics || currentWork == task.setLyrics)
                    pbProgress.Maximum = _iTunesApp.SelectedTracks.Count;
                else
                    pbProgress.Maximum = numTracks;
            });

            if (currentWork == task.noLyrics)
            {
                noLyricsPlaylist = _iTunesApp.CreatePlaylist("No lyrics (" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + ")");
                for (var i = 1; i <= numTracks; i++)
                {
                    IITTrack currTrack = tracks[i] as IITFileOrCDTrack;
                    this.BeginInvoke((Action)delegate()
                    {
                        pbProgress.Value += 1;
                    });
                    try
                    {
                        if (((IITFileOrCDTrack)currTrack).Lyrics == null && ((IITFileOrCDTrack)currTrack).VideoKind == ITVideoKind.ITVideoKindNone && ((IITFileOrCDTrack)currTrack).Podcast == false)
                            ((IITUserPlaylist)noLyricsPlaylist).AddTrack(currTrack);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            else if (currentWork == task.noArtwork)
            {
                noLyricsPlaylist = _iTunesApp.CreatePlaylist("No artwork (" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + ")");
                for (var i = 1; i <= numTracks; i++)
                {
                    this.BeginInvoke((Action)delegate()
                    {
                        pbProgress.Value += 1;
                    });
                    IITTrack currTrack = tracks[i] as IITFileOrCDTrack;
                    try
                    {
                        IITArtworkCollection artworkCollection = currTrack.Artwork;
                        if (artworkCollection.Count == 0 && ((IITFileOrCDTrack)currTrack).VideoKind == ITVideoKind.ITVideoKindNone && ((IITFileOrCDTrack)currTrack).Podcast == false)
                            ((IITUserPlaylist)noLyricsPlaylist).AddTrack(currTrack);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            else if (currentWork == task.skipWhenShuffle)
            {
                noLyricsPlaylist = _iTunesApp.CreatePlaylist("Music");
                for (var i = 1; i <= numTracks; i++)
                {
                    this.BeginInvoke((Action)delegate()
                    {
                        pbProgress.Value += 1;
                    });
                    try
                    {
                        IITTrack currTrack = tracks[i] as IITFileOrCDTrack;
                        if (!(tracks[i] as IITFileOrCDTrack).ExcludeFromShuffle && ((IITFileOrCDTrack)currTrack).VideoKind == ITVideoKind.ITVideoKindNone && ((IITFileOrCDTrack)currTrack).Podcast == false)
                        {
                            ((IITUserPlaylist)noLyricsPlaylist).AddTrack(currTrack);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            else if (currentWork == task.resetLyrics)
            {
                IITTrackCollection selectedTracks = _iTunesApp.SelectedTracks;
                for (int i = 1; i <= selectedTracks.Count; i++)
                {
                    this.BeginInvoke((Action)delegate()
                    {
                        pbProgress.Value += 1;
                    });
                    var currentTrack = (IITFileOrCDTrack)selectedTracks[i];
                    currentTrack.Lyrics = "";
                }
                this.BeginInvoke((Action)delegate()
                {
                    MessageBox.Show("Lyrics reset!", "Operation complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                });
            }
            else if (currentWork == task.setLyrics)
            {
                {
                    IITTrackCollection selectedTracks = _iTunesApp.SelectedTracks;
                    for (int i = 1; i <= selectedTracks.Count; i++)
                    {
                        this.BeginInvoke((Action)delegate()
                        {
                            pbProgress.Value += 1;
                        });
                        var currentTrack = (IITFileOrCDTrack)selectedTracks[i];
                        currentTrack.Lyrics = lyricsToSet;
                    }
                    this.BeginInvoke((Action)delegate()
                    {
                        MessageBox.Show("Lyrics set!", "Operation complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    });
                }
            }
            else if (currentWork == task.notFoundOnDisk)
            {
                noLyricsPlaylist = _iTunesApp.CreatePlaylist("Not found on disk (" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + ")");
                for (var i = 1; i <= numTracks; i++)
                {
                    this.BeginInvoke((Action)delegate()
                    {
                        pbProgress.Value += 1;
                    });
                    IITTrack currTrack = tracks[i] as IITFileOrCDTrack;
                    try
                    {
                        if ((tracks[i] as IITFileOrCDTrack).Location == null)
                        {
                            ((IITUserPlaylist)noLyricsPlaylist).AddTrack(currTrack);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            this.BeginInvoke((Action)delegate()
            {
                enableUI();
                if (currentWork == task.noLyrics || currentWork == task.noArtwork || currentWork == task.notFoundOnDisk)
                    MessageBox.Show("Playlist created!", "Operation complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            });
        }
        #endregion
        #region "UI"
        private void cbAdvanced_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbAdvanced.Checked)
            {
                pnlMore.Visible = false;
                this.Height -= 180;
            }
            else
            {
                this.Height += 180;
                pnlMore.Visible = true;
            }
        }
        private void iLyrics_FormClosing(object sender, FormClosingEventArgs e)
        {
            _iTunesApp = null;
        }
        private void Layout()
        {
            this.Text = "iLyrics by Thomas Kowalski (Version " + Application.ProductVersion + ")";
        }
        private void lblHelp_Click(object sender, EventArgs e)
        {
            Process.Start("http://bitbucket.org/thomaskowalski/ilyrics");
        } //Help link
        private void chkLyrics_CheckedChanged(object sender, EventArgs e)
        {
            cbService.Enabled = chkLyrics.Checked;
            checkCheckSongs.Enabled = chkLyrics.Checked;
            chkOverwrite.Enabled = chkLyrics.Checked;
            updateButtonText();
        } //Checkbox lyrics
        private void updateButtonText()
        {
            btnGetInfo.Enabled = true;
            if (chkLyrics.Checked && chkAlbumArt.Checked)
                btnGetInfo.Text = "Get lyrics and artwork";
            else if (!chkLyrics.Checked && chkAlbumArt.Checked)
                btnGetInfo.Text = "Get artwork";
            else if (chkLyrics.Checked && !chkAlbumArt.Checked)
                btnGetInfo.Text = "Get lyrics";
            else
                btnGetInfo.Enabled = false;
        }
        private void chkAlbumArt_CheckedChanged(object sender, EventArgs e)
        {
            cbArtworkService.Enabled = chkAlbumArt.Checked;
            cbOverwriteArtwork.Enabled = chkAlbumArt.Checked;
            updateButtonText();
        }
        private void disableUI()
        {
            foreach (Control ctl in this.Controls)
            {
                foreach (Control ctl2 in ctl.Controls)
                {
                    if (ctl2.Name != "lblHelp" && ctl2.Name != "lblCheckUpdates" && ctl2.Name != "pbProgress")
                        ctl2.Enabled = false;
                }
            }
        }
        private void enableUI()
        {
            foreach (Control ctl in this.Controls)
            {
                foreach (Control ctl2 in ctl.Controls)
                {
                    ctl2.Enabled = true;
                }
            }
            updateButtonText();
        }
        #endregion

    }
}