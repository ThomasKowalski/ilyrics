using System;
using System.Collections.Generic;
using System.Windows.Forms;
using iTunesLib;
using iLyrics.Interface;
using System.Drawing;

using System.Diagnostics;

namespace iLyrics
{
    class LyricsUpdater
    {
        private const string MSG_PROCESSING = "Processing...";
        private const string MSG_SKIPPED = "Skipped";
        private const string MSG_ARTUPDATED = "Artwork updated";
        private const string STR_TRUE = "true";
        private const string STR_FALSE = "false";
        private readonly IITTrackCollection _selectedTracks;
        private readonly FrmResult _form;
        private readonly Boolean _overwrite;
        private readonly Boolean _overwriteArtwork;
        private readonly ILyricService _lyricService;
        private readonly IArtworkService _artFinder;
        private readonly List<int> _updatedListIgnore;
        private readonly List<int> _listIgnore;
        private readonly Boolean _reCheck;
        private readonly IITPlaylist _notFoundPlaylist;
        private readonly IITPlaylist _foundPlaylist;

        public LyricsUpdater(IITTrackCollection selectedTracks, ILyricService lyricService, Boolean overwrite, Boolean overwriteArtwork, FrmResult form, IArtworkService artFinder, IITPlaylist notFoundPlaylist, IITPlaylist foundPlaylist)
        {
            _selectedTracks = selectedTracks;
            _lyricService = lyricService;
            _overwrite = overwrite;
            _overwriteArtwork = overwriteArtwork;
            _updatedListIgnore = new List<int>();
            _form = form;
            _artFinder = artFinder;
            _notFoundPlaylist = notFoundPlaylist;
            _foundPlaylist = foundPlaylist;
        }

        public void UpdateLyrics()
        {
            for (int i = 1; i <= _selectedTracks.Count; i++)
            {
                var currentTrack = (IITFileOrCDTrack)_selectedTracks[i];

                string artist = currentTrack.Artist;
                string song = currentTrack.Name;
                string album = currentTrack.Album;

                if (artist == null)
                    artist = "";
                if (album == null)
                    album = "";

                if (!string.IsNullOrEmpty(currentTrack.Location) &&
                   !string.IsNullOrEmpty(artist) && !string.IsNullOrEmpty(song))
                {
                    String[] row = { song, artist, MSG_PROCESSING };
                    var index = (int)_form.Invoke(_form.RowAdded, new Object[] { row });
                    if (_lyricService != null)
                    {
                        // If we already have lyrics and we don't want to overwrite, skip
                        if (currentTrack.Lyrics != null && !_overwrite)
                        {
                            _form.Invoke(_form.RowUpdated, new Object[] { index, MSG_SKIPPED, 2 });
                            continue;
                        }
                        // If we know no lyrics are available for this track, skip
                        int trackHashCode = currentTrack.Artist.GetHashCode() * currentTrack.ToString().GetHashCode();
                        if (_listIgnore != null && _listIgnore.IndexOf(trackHashCode) != -1 && !(_reCheck))
                        {
                            _form.Invoke(_form.RowUpdated, new Object[] { index, MSG_SKIPPED, 2 });
                            continue;
                        }

                        // In any other case:
                        try
                        {
                            // FetchLyrics will verify lyrics exist and if so, fetch them and update currentTrack
                            // It will always return a SearchResult so we know what to put in our row
                            var result = FetchLyrics(currentTrack, artist, song, index);
                            if (result == SearchResult.NotFound)
                            {
                                //remove parenthesis (and any thing inside) and retry;
                                List<String> symbols = new List<String>();
                                symbols.Add("(,)");
                                symbols.Add("[,]");
                                symbols.Add("{,}");
                                foreach (string symbolCouple in symbols)
                                {
                                    string symbol1 = symbolCouple.Split(',')[0];
                                    string symbol2 = symbolCouple.Split(',')[1];
                                    var parenStartIndex = song.IndexOf(symbol1);
                                    var parenEndIndex = song.IndexOf(symbol2);

                                    if (parenStartIndex > -1 && parenEndIndex > -1)
                                    {
                                        song = song.Remove(parenStartIndex, parenEndIndex - parenStartIndex).Trim();
                                        result = FetchLyrics(currentTrack, artist, song, index);
                                    }
                                }
                            }
                            // If track has been updated
                            if (result == SearchResult.Found) 
                            {
                                _form.Invoke(_form.RowUpdated, new Object[] { index, STR_TRUE, 2 });
                                try
                                {
                                    ((IITUserPlaylist)_foundPlaylist).AddTrack(currentTrack);
                                }
                                catch (Exception)
                                {
                                    System.Diagnostics.Debug.Print("Couldn't add track to \"Found\" playlist.");
                                }
                            }
                            else
                            {
                                try
                                {
                                    ((IITUserPlaylist)_notFoundPlaylist).AddTrack(currentTrack);
                                }
                                catch (Exception)
                                {
                                }
                                _form.Invoke(_form.RowUpdated, new Object[] { index, STR_FALSE, 2 });
                                _updatedListIgnore.Add(currentTrack.ToString().GetHashCode() * artist.ToString().GetHashCode());
                            }
                        }
                        catch (Exception ex)
                        {
                            _form.Invoke(_form.RowUpdated, new Object[] { index, ex.Message, 2 });
                        }
                    }
                    // If we want to get album artworks:
                    if (_artFinder != null)
                    {
                        try
                        {
                            IITArtworkCollection artworkCollection = currentTrack.Artwork;
                            if (artworkCollection.Count == 0 || _overwriteArtwork) //If we don't already have an artwork for that song or we want to overwrite it
                            {     
                                if (_artFinder.fetchArtwork(artist, album)) //We start the process of getting the artwork, will save it in the temp folder if it finds it
                                {
                                    currentTrack.AddArtworkFromFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iLyrics\\" + artist + " " + album + ".jpg");
                                    _form.Invoke(_form.RowUpdated, new Object[] { index, STR_TRUE, 3 });
                                }
                                else
                                    _form.Invoke(_form.RowUpdated, new Object[] { index, STR_FALSE, 3 });
                            }
                            else
                                _form.Invoke(_form.RowUpdated, new Object[] { index, "Skipped", 3 });
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("401") || ex.Message.Contains("402") || ex.Message.Contains("403"))
                            {
                                //The API Token is invalid
                                MessageBox.Show("The Genius API Key you're using is invalid! Please update it in the Settings window (you can learn how to get a new one there) and start over.", "Invalid token", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                //_form.Invoke(_form.Close);
                            } else {
                                _form.Invoke(_form.RowUpdated, new Object[] { index, ex.Message, 3 });
                            }
                        }
                    }
                }
            }
            MessageBox.Show(null, "All the tracks have been processed.", "Completed!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private SearchResult FetchLyrics(IITFileOrCDTrack currentTrack, string artist, string song, int index)
        {
            String url;
            if (_lyricService.Exists(artist, song, out url) == SearchResult.NotFound)
            {
                Debug.Print("Couldn't find lyrics for \"" + song + "\" (by " + artist + ") with service '" + _lyricService.GetName() + ".");
                return SearchResult.NotFound;
            }

            String lyrics;
            var result = _lyricService.GetLyrics(artist, song, out lyrics, url);

            if (result == SearchResult.Found)
                currentTrack.Lyrics = lyrics;

            return result;
        }
    }
}