using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using iTunesLib;
using iLyrics.Interface;

namespace iLyrics
{
    public delegate int AddRowDelegate(String[] row);

    public delegate void UpdateRowDelegate(int index, string Result, int columnToUpdate);

    public partial class FrmResult : Form
    {
        private const string STR_TRUE = "true";
        private const string STR_FALSE = "false";
        private const string MSG_UPDATED = "Updated";
        private const string MSG_NOT_FOUND = "Not Found";
        private const string MSG_NO_SONG_FOUND = "No matching song found";

        private readonly ILyricService _lyricService;
        private readonly IArtworkService _artFinder;
        private readonly IITTrackCollection _selectedTracks;

        private readonly Boolean _overwrite;
        private readonly Boolean _overwriteArtwork;
        private readonly IITPlaylist _notFoundPlaylist;
        private readonly IITPlaylist _foundPlaylist;

        // Delegate instances used to call user interface
        // functions from worker thread:
        public AddRowDelegate RowAdded;
        public UpdateRowDelegate RowUpdated;

        public FrmResult(IITTrackCollection selectedTracks, ILyricService lyricsService, Boolean overwrite, Boolean overwriteArtwork, IArtworkService artFinder, IITPlaylist notFoundPlaylist, IITPlaylist foundPlaylist)
            : this()
        {
            _selectedTracks = selectedTracks;
            _lyricService = lyricsService;
            _artFinder = artFinder;
            _overwrite = overwrite;
            _overwriteArtwork = overwriteArtwork;
            _notFoundPlaylist = notFoundPlaylist;
            _foundPlaylist = foundPlaylist;
        }

        public FrmResult()
        {
            InitializeComponent();

            // initialize delegates
            RowAdded = new AddRowDelegate(AddRow);
            RowUpdated = new UpdateRowDelegate(UpdateRow);
        }

        private void frmResult_Load(object sender, EventArgs e)
        {
            var lu = new LyricsUpdater(_selectedTracks, _lyricService, _overwrite, _overwriteArtwork, this, _artFinder, _notFoundPlaylist, _foundPlaylist);

            for (int i = 0; i < mainDGV.Columns.Count - 1; i++)
            {
                mainDGV.Columns[i].SortMode = DataGridViewColumnSortMode.Programmatic;
            }

            ThreadStart threadDelegate = lu.UpdateLyrics;
            var newThread = new Thread(threadDelegate);
            newThread.Start();
        }

        private int AddRow(String[] row)
        {
            int index = mainDGV.Rows.Add(row);
            if (chkAutoScroll.Checked)
            {
                mainDGV.FirstDisplayedScrollingRowIndex = index;
            }
            return index;
        }

        private void UpdateRow(int index, string Result, int columnToUpdate)
        {
            if (Result == STR_TRUE)
            {
                mainDGV.Rows[index].Cells[columnToUpdate].Value = MSG_UPDATED;
            }
            else if (Result == STR_FALSE)
            {
                mainDGV.Rows[index].Cells[columnToUpdate].Value = MSG_NOT_FOUND;
                mainDGV.Rows[index].ErrorText = MSG_NO_SONG_FOUND;
            }
            else
            {
                mainDGV.Rows[index].Cells[columnToUpdate].Value = Result;
                mainDGV.Rows[index].ErrorText = Result;
                mainDGV.Rows[index].DefaultCellStyle.BackColor = Color.YellowGreen;
            }
            if (chkAutoScroll.Checked)
            {
                mainDGV.FirstDisplayedScrollingRowIndex = index;
            }
        }
    }
}