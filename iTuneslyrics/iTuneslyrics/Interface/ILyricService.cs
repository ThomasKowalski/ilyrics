namespace iLyrics.Interface
{
    public interface ILyricService
    {
        SearchResult Exists(string artist, string song, out string url);
        SearchResult GetLyrics(string artist, string song, out string lyrics, string url = null);
        string GetName();
    }
}