using System.Drawing;
namespace iLyrics.Interface
{
    public interface IArtworkService
    {
        bool fetchArtwork(string artist, string album);
        Image findCover(string artist, string song, string album);
        string GetName();
    }
}