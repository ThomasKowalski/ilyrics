![1.PNG](https://bitbucket.org/repo/LKjeRb/images/774935852-1.PNG)

# Get started! #

Hey!
How to use this? Well, download the ZIP file, extract all of its contents in the same folder, start `iLyrics.exe`, and it should work! It doesn't? [Please open an issue here.](https://bitbucket.org/ThomasKowalski/ilyrics/issues/new)

#[Download](https://bitbucket.org/ThomasKowalski/ilyrics/raw/master/iTuneslyrics/iTuneslyrics/bin/Debug/iLyrics.zip) (Version 3.13)#

[Changelog](https://bitbucket.org/ThomasKowalski/ilyrics/raw/master/iTuneslyrics/iTuneslyrics/bin/Debug/changelog.txt)

# Help (aka what are those check boxes?) #

## What is "Get Lyrics"? ##
It was the first use of this software: getting lyrics for your iTunes tracks. You can choose to use MediaWiki (using its official API - the thing I use to get lyrics from the site -, which is really good since it's not using some random tweaks), Genius, which has an official search API but no official lyrics fetching API (so the results should always be OK, unless some rare cases) or AZLyrics, which has no API at all, and less songs. Your choice! If you don't know, use the Automatic option, which will try each of them.
When you click the button, you'll have two options : you can ask iLyrics to create a playlist containing the songs it found lyrics for, a playlist containing songs it couldn't find lyrics for, or both. This is useful in case you want to verify afterwards that the lyrics that were found are okay, and to get lyrics manually if they weren't found.

## What is "Get Artwork"? ##
Well, as strange as it can seem, this will allow you to get album artwork for your tracks! iLyrics uses allcdcovers.com, using the custom API I specially created for it. If you want to use this API, just download the class or email me if you want help about how to use it.

Here, just an example of those features (using the Manual Update):

![Manual.PNG](https://bitbucket.org/repo/LKjeRb/images/2039516013-Manual.PNG)

## What is "Automatic update"? ##
It will update the lyrics of all selected songs without asking for your confirmation:

![Automatic.PNG](https://bitbucket.org/repo/LKjeRb/images/1247403484-Automatic.PNG)

## What is "Overwrite"? ##
If checked, it will remove the lyrics of selected songs and replace them with the ones it will find online. If it doesn't find any, it will keep the old ones.

## What is "Reverify songs"? ##
iLyrics will create a cache of songs for which it didn't find lyrics (so if you start a new search, it won't bother trying to find lyrics for them). If this box is checked, it will recreate the cache and verify the availability of all the selected songs.

## What are the other buttons? ##
The other buttons (you can get them by ticking "Show advanced") can be really useful.

Note: if you want the buttons to only scan some files of the library, just select them in iTunes, and then press the button you want to use. iLyrics will ask for a confirmation about whether or not you want to scan only the files that you selected.

### Generate "No lyrics playlist" and Generate "No Artwork" playlist ###
These will create an iTunes playlist containing all the tracks that don't have lyrics (for the first button) or artwork (for the second button).

### Set lyrics to selected ###
If you have some classical music, you probably don't have lyrics for them. If you just want to put "Instrumental" lyrics for all of them, select them, click the button, and enter whatever you want. Each of them will have the same lyrics.

### Create "Not found on disk" playlist ###
This will create an iTunes playlist containing all the tracks that could not be found on disk.

### Set lyrics to selected ###
This will allow you to set the lyrics to the same things for all the selected songs. For example, if you have classical music, use this to set all the songs to "Instrumental". 

### Reset lyrics ###
This will allow you to remove lyrics for the selected songs. It cannot be reverted, use with care!

### Show console ###
![Console.PNG](https://bitbucket.org/repo/LKjeRb/images/3303681338-Console.PNG)

This will show the **experimental** console. To learn more about it, read [this wiki article](https://bitbucket.org/ThomasKowalski/ilyrics/wiki/Using%20the%20console).

## How to get updates? ##
Simply click on the text asking if you want cool features. It will check for new versions and propose you to download it if there's any. If you choose to update, your web browser will open to this page, you'll just have to download the latest archive.

# FAQ #

## I get an error saying "This track is not modifiable" ##
iTunes will trigger this when the file for which you're trying to get lyrics is marked read-only in Windows. To modify this, right-click on the song causing the problem in iTunes, click "Show in Explorer", right-click on it and select Properties (or select it in the explorer and use Alt + Enter) and uncheck "Read-only". If multiple tracks trigger this, you can select all of them and do it once, or you can select the parent folder, click on "Read-only" in its properties until it is totally unchecked (not a square, not a tick, it has to be empty) and then choose to apply to all the elements contained in this folder.

## How to I get lyrics on my iPod? ##
![photo.PNG](https://bitbucket.org/repo/LKjeRb/images/2601721735-photo.PNG)

If your iPod does not show lyrics, go to Settings, Music, Lyrics & Podcast Info, and turn them on (not sure it works in iOS 10, though...)

## What services are supported? ##
Currently, three lyrics services are supported:

* LyricWiki

* Genius

* AZLyrics

If you don't know what to choose, use (Automatic). It will try each service, starting by Genius (best quality) and finishing by AZLyrics (lowest quality, and less large catalog).

Only one album artwork service is usable in the current state, which is Deezer. It works perfectly, so you shouldn't have to use anything else.


## Other services? ##
You could suggest me new services. Though, please note that:

* MusixMatch

* GreatSong

* Paroles-Musique

* MetroLyrics

won't be supported because they don't have a (free) API and parsing their HTML code is really, really difficult (I'm pretty sure they don't want me or that kind of software to use their data).

## Can I "steal" your X or Y class? ##
Yeah, as long as you say it was created by me, and include a link to this project if you distribute it. 
You shall not use it for commercial use, though.

## It stops working at launch ##
Please download the iTunes Interop Library in the first section and place it in the same directory than the app.

## It won't launch when I launch it ##
Verify iTunes is launched. If it still doesn't work with iTunes open, contact me.

## What's to expect in the future? ##

* [Bug fixes](https://bitbucket.org/ThomasKowalski/ilyrics/issues?status=new&status=open)
* HTML Tags in lyrics. I noticed randomly that iOS parses the lyrics as HTML code, so I could add bold tags to the [Hook] / [Verse] / ... thingies in the Genius lyrics (which would be optionnal of course). It's not a huge feature but it'd be cool.
* AllCDCovers support